package biz.noxus.geotaggingsurvey.utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by Rangga on 11/18/2015.
 */
public class ContextWrapper extends Application {

    private static Context mContext;

    public static Context getContext(){
        return mContext;
    }

    public static void setContext(Context mContext1){
        mContext = mContext1;
    }

}
