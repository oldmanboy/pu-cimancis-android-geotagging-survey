package biz.noxus.geotaggingsurvey.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class PrefUtils {

    public static final String PREF_NAME = "TempData";
    public static final String SAVE_STATE ="";
    public static final String PREF_PREVIOUS_DATA = "hasPreviousData";
    public static final String PREF_LOGIN_STATE = "false";
    public static final String PREF_LOGIN_USER = "";
    public static final String PREF_LOGIN_ROLE = "";
    public static final String PREF_BASE_URL = "";

    public static void init(final Context context) {
    }

    public static void saveToPref(final Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        sp.edit().putString("This",SAVE_STATE).commit();
    }

    public static void resetPref(final Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        sp.edit().clear().commit();
        Log.d("Pref Utils tracker", "pref reset is called");
    }

    public static String getPrefName(){
        return PREF_NAME;
    }

    public static void setLoggedIn(Context context){
        SharedPreferences sp = context.getSharedPreferences("LOGIN_DATA", Context.MODE_PRIVATE);
        sp.edit().putBoolean(PREF_LOGIN_STATE, true).commit();
    }

    public static void setLoggedInUser(Context context, String userName){
        SharedPreferences sp = context.getSharedPreferences("LOGIN_DATA", Context.MODE_PRIVATE);
        sp.edit().putString(PREF_LOGIN_USER, userName).commit();
        Log.d("Logged in user is ", userName);
    }

    public static String getLoggedInUser(Context context){
        SharedPreferences sp = context.getSharedPreferences("LOGIN_DATA", Context.MODE_PRIVATE);
        Log.d("Logged in user is ",PREF_LOGIN_USER);
        return sp.getString(PREF_LOGIN_USER, "-");
    }

    public static void setLoggedInUserRole(Context context, String userRole){
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        sp.edit().putString(PREF_LOGIN_ROLE, userRole).commit();
    }


    public static void setNoPreviousData(Context context){
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        sp.edit().putBoolean(PREF_PREVIOUS_DATA, false).commit();
    }

    public static void setPreviousDataFound(Context context){
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        sp.edit().putBoolean(PREF_PREVIOUS_DATA, true).commit();
    }

    public static Boolean getPreviousDataStatus(Context context){
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sp.getBoolean(PREF_PREVIOUS_DATA,false);
    }

    public static void setThrownIdForDBSearching(Context context, String buildingId){
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        sp.edit().putString("idOfBuilding",buildingId).commit();
    }

    public static String getThrownIdForDBSearching(Context context){
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sp.getString("idOfBuilding","-");
    }
}
