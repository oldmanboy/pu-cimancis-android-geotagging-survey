package biz.noxus.geotaggingsurvey.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name="User")
public class User extends Model{

    @Column(name = "user_id")
    public String userId;

    @Column(name = "user_password")
    public String userPassword;

    @Column(name = "role")
    public String role;

    public User(){super();}

}
