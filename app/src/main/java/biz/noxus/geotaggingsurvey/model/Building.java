package biz.noxus.geotaggingsurvey.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Buildings")
public class Building extends Model {

    @Column(name = "building_id")
    public String buildingId;

    @Column(name = "building_type")
    public String buildingType;

    @Column(name = "geoObject_group_id")
    public Integer geoObjectGroupId;

    @Column(name = "foto_progress_beginning")
    public String fotoProgressBeginning;

    @Column(name = "foto_progress_middle")
    public String fotoProgressMiddle;

    @Column(name = "foto_progress_end")
    public String fotoProgressEnd;

    @Column(name = "uploaded_state")
    public Boolean isUploaded;
/*
    @Column(name = "nama_desa")
    public String nama_desa;

    @Column(name = "nama_kecamatan")
    public String nama_kecamatan;

    @Column(name = "nama_kabkota")
    public String nama_kabkota;

    @Column(name = "nama_provinsi")
    public String nama_provinsi;

    @Column(name = "program")
    public String program;

    @Column(name = "pekerjaan")
    public String pekerjaan;

    @Column(name = "kegiatan")
    public String kegiatan;
*/
    @Column(name = "pelaksana")
    public String pelaksana;

    @Column(name = "param1")
    public String param1;

    @Column(name = "param2")
    public String param2;

    @Column(name = "param3")
    public String param3;

    @Column(name = "param4")
    public String param4;

    @Column(name = "param5")
    public String param5;

    @Column(name = "param6")
    public String param6;

    @Column(name = "param7")
    public String param7;

    @Column(name = "param8")
    public String param8;

    @Column(name = "param9")
    public String param9;

    @Column(name = "param10")
    public String param10;

    @Column(name = "param11")
    public String param11;

    @Column(name = "param12")
    public String param12;

    @Column(name = "param13")
    public String param13;

    @Column(name = "param14")
    public String param14;

    @Column(name = "param15")
    public String param15;

    @Column(name = "param16")
    public String param16;

    @Column(name = "param17")
    public String param17;

    @Column(name = "param18")
    public String param18;

    @Column(name = "param19")
    public String param19;

    @Column(name = "param20")
    public String param20;

    @Column(name = "param21")
    public String param21;

    @Column(name = "param22")
    public String param22;

    @Column(name = "param23")
    public String param23;

    @Column(name = "param24")
    public String param24;

    @Column(name = "param25")
    public String param25;

    @Column(name = "param26")
    public String param26;

    @Column(name = "param27")
    public String param27;

    @Column(name = "param28")
    public String param28;

    @Column(name = "param29")
    public String param29;

    @Column(name = "param30")
    public String param30;

    @Column(name = "param31")
    public String param31;

    @Column(name = "param32")
    public String param32;

    @Column(name = "param33")
    public String param33;

    @Column(name = "param34")
    public String param34;

    @Column(name = "param35")
    public String param35;

    @Column(name = "param36")
    public String param36;

    @Column(name = "param37")
    public String param37;

    @Column(name = "param38")
    public String param38;

    @Column(name = "param39")
    public String param39;

    @Column(name = "param40")
    public String param40;

    @Column(name = "param41")
    public String param41;

    @Column(name = "param42")
    public String param42;

    @Column(name = "param43")
    public String param43;

    @Column(name = "param44")
    public String param44;

    @Column(name = "param45")
    public String param45;

    @Column(name = "param46")
    public String param46;

    @Column(name = "param47")
    public String param47;

    @Column(name = "param48")
    public String param48;

    @Column(name = "param49")
    public String param49;

    @Column(name = "param50")
    public String param50;

    @Column(name = "param51")
    public String param51;

    @Column(name = "param52")
    public String param52;

    @Column(name = "param53")
    public String param53;

    @Column(name = "param54")
    public String param54;

    @Column(name = "param55")
    public String param56;

    @Column(name = "param57")
    public String param57;

    @Column(name = "param58")
    public String param58;

    @Column(name = "param59")
    public String param59;

    @Column(name = "param60")
    public String param60;


    public Building() {
        super();
    }

    public Building(String buildingId,
                    String buildingType,
                    Integer geoObjectGroupId,
                    String fotoProgressBeginning,
                    String fotoProgressMiddle,
                    String fotoProgressEnd,
                    Boolean isUploaded,
                    String nama_desa,
                    String nama_kecamatan,
                    String nama_kabkota,
                    String nama_provinsi,
                    String program,
                    String pekerjaan,
                    String kegiatan
    ) {
        super();
    }

    /*
    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public Integer getGeoObjectGroupId() {
        return geoObjectGroupId;
    }

    public void setGeoObjectGroupId(Integer geoObjectGroupId) {
        this.geoObjectGroupId = geoObjectGroupId;
    }

    public String getFotoProgressBeginning() {
        return fotoProgressBeginning;
    }

    public void setFotoProgressBeginning(String fotoProgressBeginning) {
        this.fotoProgressBeginning = fotoProgressBeginning;
    }

    public String getFotoProgressMiddle() {
        return fotoProgressMiddle;
    }

    public void setFotoProgressMiddle(String fotoProgressMiddle) {
        this.fotoProgressMiddle = fotoProgressMiddle;
    }

    public String getFotoProgressEnd() {
        return fotoProgressEnd;
    }

    public void setFotoProgressEnd(String fotoProgressEnd) {
        this.fotoProgressEnd = fotoProgressEnd;
    }

    public Boolean getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(Boolean isUploaded) {
        this.isUploaded = isUploaded;
    }

    public String getNama_desa() {
        return nama_desa;
    }

    public void setNama_desa(String nama_desa) {
        this.nama_desa = nama_desa;
    }

    public String getNama_kecamatan() {
        return nama_kecamatan;
    }

    public void setNama_kecamatan(String nama_kecamatan) {
        this.nama_kecamatan = nama_kecamatan;
    }

    public String getNama_kabkota() {
        return nama_kabkota;
    }

    public void setNama_kabkota(String nama_kabkota) {
        this.nama_kabkota = nama_kabkota;
    }

    public String getNama_provinsi() {
        return nama_provinsi;
    }

    public void setNama_provinsi(String nama_provinsi) {
        this.nama_provinsi = nama_provinsi;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getKegiatan() {
        return kegiatan;
    }

    public void setKegiatan(String kegiatan) {
        this.kegiatan = kegiatan;
    }
    */
}
