package biz.noxus.geotaggingsurvey.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by Rangga on 11/17/2015.
 */
@Table(name = "PAI")
public class PAI extends Model {

    @Column(name = "PAI_id")
    public String id;

    @Column(name = "asset_type")
    public String assetType;

    @Column(name = "pelaksana")
    public String pelaksana;

    @Column(name = "param1")
    public String param1;

    @Column(name = "param2")
    public String param2;

    @Column(name = "param3")
    public String param3;

    @Column(name = "param4")
    public String param4;

    @Column(name = "param5")
    public String param5;

    @Column(name = "param6")
    public String param6;

    @Column(name = "param7")
    public String param7;

    @Column(name = "param8")
    public String param8;

    @Column(name = "param9")
    public String param9;

    @Column(name = "param10")
    public String param10;

    @Column(name = "param11")
    public String param11;

    @Column(name = "param12")
    public String param12;

    @Column(name = "param13")
    public String param13;

    @Column(name = "param14")
    public String param14;

    @Column(name = "param15")
    public String param15;

    @Column(name = "param16")
    public String param16;

    @Column(name = "param17")
    public String param17;

    @Column(name = "param18")
    public String param18;

    @Column(name = "param19")
    public String param19;

    @Column(name = "param20")
    public String param20;

    @Column(name = "param21")
    public String param21;

    @Column(name = "param22")
    public String param22;

    @Column(name = "param23")
    public String param23;

    @Column(name = "param24")
    public String param24;

    @Column(name = "param25")
    public String param25;

    @Column(name = "param26")
    public String param26;

    @Column(name = "param27")
    public String param27;

    @Column(name = "param28")
    public String param28;

    @Column(name = "param29")
    public String param29;

    @Column(name = "param30")
    public String param30;

    @Column(name = "param31")
    public String param31;

    @Column(name = "param32")
    public String param32;

    @Column(name = "param33")
    public String param33;

    @Column(name = "param34")
    public String param34;

    @Column(name = "param35")
    public String param35;

    @Column(name = "param36")
    public String param36;

    @Column(name = "param37")
    public String param37;

    @Column(name = "param38")
    public String param38;

    @Column(name = "param39")
    public String param39;

    @Column(name = "param40")
    public String param40;

    @Column(name = "param41")
    public String param41;

    @Column(name = "param42")
    public String param42;

    @Column(name = "param43")
    public String param43;

    @Column(name = "param44")
    public String param44;

    @Column(name = "param45")
    public String param45;

    public PAI() {
        super();
    }

}


