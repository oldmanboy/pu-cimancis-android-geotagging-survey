package biz.noxus.geotaggingsurvey.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "GeoObjects")
public class GeoObject extends Model {

    @Column(name = "group_object_id")
    public Integer groupObjectId;

    @Column(name = "group_object_type")
    public String groupObjectType;

    @Column(name = "group_object_no")
    public Integer groupObjectNo;

    @Column(name = "koordinat_latitude")
    public String koordinatLatitude;

    @Column(name = "koordinat_longitude")
    public String koordinatLongitude;

    @Column(name = "koordinat_elevasi")
    public String koordinatElevasi;

    @Column(name = "isUploaded")
    public Boolean isUploaded;

    @Column(name = "pelaksana")
    public String pelaksana;

    public GeoObject() {
        super();
    }

    public GeoObject(Integer groupObjectId,
                     String groupObjectType,
                     Integer groupObjectNo,
                     String koordinatLatitude,
                     String koordinatLongitude,
                     String koordinatElevasi,
                     Boolean isUploaded
    ) {
        super();
    }

    public Integer getGroupObjectId() {
        return groupObjectId;
    }

    public void setGroupObjectId(Integer groupObjectId) {
        this.groupObjectId = groupObjectId;
    }

    public String getGroupObjectType() {
        return groupObjectType;
    }

    public void setGroupObjectType(String groupObjectType) {
        this.groupObjectType = groupObjectType;
    }

    public Integer getGroupObjectNo() {
        return groupObjectNo;
    }

    public void setGroupObjectNo(Integer groupObjectNo) {
        this.groupObjectNo = groupObjectNo;
    }

    public String getKoordinatLatitude() {
        return koordinatLatitude;
    }

    public void setKoordinatLatitude(String koordinatLatitude) {
        this.koordinatLatitude = koordinatLatitude;
    }

    public String getKoordinatLongitude() {
        return koordinatLongitude;
    }

    public void setKoordinatLongitude(String koordinatLongitude) {
        this.koordinatLongitude = koordinatLongitude;
    }

    public String getKoordinatElevasi() {
        return koordinatElevasi;
    }

    public void setKoordinatElevasi(String koordinatElevasi) {
        this.koordinatElevasi = koordinatElevasi;
    }

    public Boolean getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(Boolean isUploaded) {
        this.isUploaded = isUploaded;
    }
}
