package biz.noxus.geotaggingsurvey.helper;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import java.util.HashMap;

/**
 * Created by Rangga on 11/20/2015.
 */
public class BulkDataLoader {

    public Integer foundEditText = 0;
    public String Modifier = "param";

    private SharedPreferences.Editor bulkEditor;
    private Cursor bulkCursor;
    private HashMap bulkHashmap = new HashMap();


    public BulkDataLoader() {

    }

    public void floodEditTextWithWords(View masterView, int targetResourceViewId) {
        ViewGroup parentView = (ViewGroup) masterView.findViewById(targetResourceViewId);
foundEditText =0;
        for (int i = 0; i < parentView.getChildCount(); i++) {
            View childView = parentView.getChildAt(i);
            Integer counter = i;

            if (childView instanceof LinearLayout) {
                ViewGroup subParentView = (ViewGroup) childView;
                for (int j = 0; j < subParentView.getChildCount(); j++) {
                    View subChildView = subParentView.getChildAt(j);

                    Integer subCounter = i;
                    if (subChildView instanceof EditText) {
                        this.foundEditText += 1;

                        try {
                            ((com.rengwuxian.materialedittext.MaterialEditText) subChildView).setText("Input Sub Text target " + this.foundEditText);
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
//                        Log.d("sub edit text count is", subCounter.toString());

                    }

                    // filter ini digunakan saat bertemu dengan table layout
                    if (subChildView instanceof HorizontalScrollView) {
                        View thisShouldBeATable = ((HorizontalScrollView) subChildView).getChildAt(0);
                        ViewGroup thisTableViewGroup = (ViewGroup) thisShouldBeATable;
                        Integer howMuchRow = thisTableViewGroup.getChildCount();

                        for (int x = 1; x < howMuchRow; x++) {
                            View thisShouldBeATableRow = thisTableViewGroup.getChildAt(x);
                            scrollThroughForEditText(thisShouldBeATableRow,"flood with text");
                        }


                        Integer counterOfWhatsInIt = ((HorizontalScrollView) subChildView).getChildCount();
                        Log.d("flood found some", counterOfWhatsInIt.toString());

                    }


                    Log.d("flood found some", subChildView.toString());

                }
            }
            if (childView instanceof EditText) {
                this.foundEditText += 1;
                try {
                    ((com.rengwuxian.materialedittext.MaterialEditText) childView).setText("Input Text target " + this.foundEditText);
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                Log.d("edit text count is", counter.toString());

            }
        }
        Log.d("flood found edit text", this.foundEditText.toString());

    }

    public void loadEditTextToPrefEditor(View masterView, int targetResourceViewId, SharedPreferences.Editor editor) {
        ViewGroup parentView = (ViewGroup) masterView.findViewById(targetResourceViewId);
bulkEditor = editor;
        foundEditText =0;
        for (int i = 0; i < parentView.getChildCount(); i++) {
            View childView = parentView.getChildAt(i);
            Integer counter = i;

            if (childView instanceof LinearLayout) {
                ViewGroup subParentView = (ViewGroup) childView;
                for (int j = 0; j < subParentView.getChildCount(); j++) {
                    View subChildView = subParentView.getChildAt(j);

                    Integer subCounter = i;
                    if (subChildView instanceof EditText) {
                        this.foundEditText += 1;
                        // put some code here
                        editor.putString("param" + this.foundEditText, ((com.rengwuxian.materialedittext.MaterialEditText) subChildView).getText().toString());
                        Log.d("loadedpref edit text", ((com.rengwuxian.materialedittext.MaterialEditText) subChildView).getText().toString());

                    }

                    // filter ini digunakan saat bertemu dengan table layout
                    if (subChildView instanceof HorizontalScrollView) {
                        View thisShouldBeATable = ((HorizontalScrollView) subChildView).getChildAt(0);
                        ViewGroup thisTableViewGroup = (ViewGroup) thisShouldBeATable;
                        Integer howMuchRow = thisTableViewGroup.getChildCount();

                        for (int x = 1; x < howMuchRow; x++) {
                            View thisShouldBeATableRow = thisTableViewGroup.getChildAt(x);
                            scrollThroughForEditText(thisShouldBeATableRow," put into pref");
                        }


                        Integer counterOfWhatsInIt = ((HorizontalScrollView) subChildView).getChildCount();
                        Log.d("loadpref found some", counterOfWhatsInIt.toString());

                    }

                }
            }
            if (childView instanceof EditText) {
                this.foundEditText += 1;
                // put some code here
                editor.putString("param" + this.foundEditText, ((com.rengwuxian.materialedittext.MaterialEditText) childView).getText().toString());
                Log.d("loadpref main edit text", counter.toString());

            }
        }
        editor.putInt("textEditTotalCount", this.foundEditText);
    }

    public void loadEditTextWithWordsFromDB(View masterView, int targetResourceViewId, Cursor thrownCursor) {
        ViewGroup parentView = (ViewGroup) masterView.findViewById(targetResourceViewId);
        String modifier = "param";
        bulkCursor = thrownCursor;
        foundEditText =0;
        for (int i = 0; i < parentView.getChildCount(); i++) {
            View childView = parentView.getChildAt(i);
            Integer counter = i;

            if (childView instanceof LinearLayout) {
                ViewGroup subParentView = (ViewGroup) childView;
                for (int j = 0; j < subParentView.getChildCount(); j++) {
                    View subChildView = subParentView.getChildAt(j);

                    Integer subCounter = i;
                    if (subChildView instanceof EditText) {
                        this.foundEditText += 1;
                        ((com.rengwuxian.materialedittext.MaterialEditText) subChildView).setText(thrownCursor.getString(thrownCursor.getColumnIndexOrThrow(modifier + String.valueOf(this.foundEditText))));
//                        Log.d("what happened is", pointerToData);

                    }

                    // filter ini digunakan saat bertemu dengan table layout
                    if (subChildView instanceof HorizontalScrollView) {
                        View thisShouldBeATable = ((HorizontalScrollView) subChildView).getChildAt(0);
                        ViewGroup thisTableViewGroup = (ViewGroup) thisShouldBeATable;
                        Integer howMuchRow = thisTableViewGroup.getChildCount();

                        for (int x = 1; x < howMuchRow; x++) {
                            View thisShouldBeATableRow = thisTableViewGroup.getChildAt(x);
                            scrollThroughForEditText(thisShouldBeATableRow, "load from db");
                        }


                        Integer counterOfWhatsInIt = ((HorizontalScrollView) subChildView).getChildCount();
                        Log.d("loaddb found some", counterOfWhatsInIt.toString());

                    }

                }
            }
            if (childView instanceof EditText) {
                this.foundEditText += 1;
                ((com.rengwuxian.materialedittext.MaterialEditText) childView).setText(thrownCursor.getString(thrownCursor.getColumnIndexOrThrow(modifier + String.valueOf(this.foundEditText))));
                Log.d("loaddb edit text count", counter.toString());

            }
        }
    }

    public HashMap putIntoHashMap(View masterView, int targetResourceViewId) {

        ViewGroup parentView = (ViewGroup) masterView.findViewById(targetResourceViewId);
this.foundEditText = 0;
        for (int i = 0; i < parentView.getChildCount(); i++) {
            View childView = parentView.getChildAt(i);
            Integer counter = i;

            if (childView instanceof LinearLayout) {
                ViewGroup subParentView = (ViewGroup) childView;
                for (int j = 0; j < subParentView.getChildCount(); j++) {
                    View subChildView = subParentView.getChildAt(j);

                    Integer subCounter = i;
                    if (subChildView instanceof EditText) {
                        this.foundEditText += 1;
                        // put some code here
                        bulkHashmap.put("param" + this.foundEditText, ((com.rengwuxian.materialedittext.MaterialEditText) subChildView).getText().toString());
                        Log.d("hash subedit text", subCounter.toString());

                    }

                    // filter ini digunakan saat bertemu dengan table layout
                    if (subChildView instanceof HorizontalScrollView) {
                        View thisShouldBeATable = ((HorizontalScrollView) subChildView).getChildAt(0);
                        ViewGroup thisTableViewGroup = (ViewGroup) thisShouldBeATable;
                        Integer howMuchRow = thisTableViewGroup.getChildCount();

                        for (int x = 1; x < howMuchRow; x++) {
                            View thisShouldBeATableRow = thisTableViewGroup.getChildAt(x);
                            scrollThroughForEditText(thisShouldBeATableRow, "set into hashmap");
                        }


                        Integer counterOfWhatsInIt = ((HorizontalScrollView) subChildView).getChildCount();
                        Log.d("hash subedit found", counterOfWhatsInIt.toString());

                    }

                }
            }
            if (childView instanceof EditText) {
                this.foundEditText += 1;
                // put some code here
                bulkHashmap.put("param" + this.foundEditText, ((com.rengwuxian.materialedittext.MaterialEditText) childView).getText().toString());
                Log.d("hash main edit text", counter.toString());

            }
        }
        bulkHashmap.put("textEditTotalCount", this.foundEditText);
        Log.d("total count", this.foundEditText.toString());
        return bulkHashmap;
    }

    public void scrollThroughForEditText(View masterView, String type) {
        ViewGroup parentView = (ViewGroup) masterView;
        for (int i = 0; i < parentView.getChildCount(); i++) {
            View childView = parentView.getChildAt(i);
            Integer counter = i;

            if (childView instanceof EditText) {
                this.foundEditText += 1;
                // put some code here
                switch (type){
                    case "flood with text":((com.rengwuxian.materialedittext.MaterialEditText) childView).setText("Input Text target " + this.foundEditText);
                        break;
                    case "put into pref": bulkEditor.putString("param" + this.foundEditText, ((com.rengwuxian.materialedittext.MaterialEditText) childView).getText().toString());
                        break;
                    case "load from db":  ((com.rengwuxian.materialedittext.MaterialEditText) childView).setText(bulkCursor.getString(bulkCursor.getColumnIndexOrThrow(Modifier + String.valueOf(this.foundEditText))));
                        break;
                    case "set into hashmap":bulkHashmap.put("param" + this.foundEditText, ((com.rengwuxian.materialedittext.MaterialEditText) childView).getText().toString());
                        break;
                }


                Log.d("loaded subedit text", counter.toString());

            }

            Log.d("found some", childView.toString());

        }
    }



}
