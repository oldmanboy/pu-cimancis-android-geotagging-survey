package biz.noxus.geotaggingsurvey.helper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import biz.noxus.geotaggingsurvey.utils.PrefUtils;

public class FragmentHelper {

    public static void init(final Context context) {
        //TODO: fill in this if needed
    }

    public static void intentStateCheck(final Context context, final Intent intent){
        //if the bundle is not empty
        //TODO: get detail from database to put in the current prefs for user to update
        Bundle bundleFromSavedData = intent.getExtras();
        clearPrefState(context);
        if (bundleFromSavedData != null) {

           PrefUtils.setPreviousDataFound(context);
           String idFromBundle = bundleFromSavedData.get("itemFromDatabaseIndex").toString();
           PrefUtils.setThrownIdForDBSearching(context, idFromBundle);

           Toast.makeText(context,
                    "Previous Data Found. ID : " + bundleFromSavedData.get("itemFromDatabaseIndex"), Toast.LENGTH_SHORT).show();
        }

        else {
            PrefUtils.setNoPreviousData(context);
            Toast.makeText(context,
                    "No Previous Data Found ", Toast.LENGTH_SHORT).show();
        }
    }

    private static void clearPrefState(Context context){
        PrefUtils.resetPref(context);
    }

}
