package biz.noxus.geotaggingsurvey.helper;

public class DataPacket {
	//private variables
	int _id;
	String _nama, _alamat;
	String _tipeAirMinum, _tipeAirLimbah, _tipePersampahan, _tipeDrainase;
	String _kategori;
		//blon final untuk tipe data yang dipakainya
	String _lat, _lon , _lokasi;
	String _created_at;
	String _created_by;
		//image container x4
	String _imgAirMinum, _imgAirLimbah, _imgPersampahan, _imgDrainase;
		//image path x4
	String _imgPathAirMinum, _imgPathAirLimbah, _imgPathPersampahan, _imgPathDrainase;
	
	String _isProcessed;
	
	//constructor
	//empty constructor
	public DataPacket(){
		
	}
	
	//constructor with id
	public DataPacket(int id, String tipeAirMinum, String tipeAirLimbah, String tipePersampahan, String tipeDrainase,
			String imgPathAirMinum, String imgPathAirLimbah, String imgPathPersampahan, String imgPathDrainase,
			String nama, String alamat, String lat, String lon, String isProcessed){
		this._id = id;
		this._tipeAirMinum = tipeAirMinum;
		this._tipeAirLimbah = tipeAirLimbah;
		this._tipePersampahan = tipePersampahan;
		this._tipeDrainase = tipeDrainase;
		this._imgPathAirMinum = imgPathAirMinum;
		this._imgPathAirLimbah = imgPathAirLimbah;
		this._imgPathPersampahan = imgPathPersampahan;
		this._imgPathDrainase = imgPathDrainase;
		//this._kategori = kategori;
		//this._created_at = created_at;
		this._nama = nama;
		this._alamat = alamat;
		this._lat = lat;
		this._lon = lon;
		this._isProcessed = isProcessed;
	}
	
	//constructor without id
	public DataPacket(String tipeAirMinum, String tipeAirLimbah, String tipePersampahan, String tipeDrainase,
			String imgPathAirMinum, String imgPathAirLimbah, String imgPathPersampahan, String imgPathDrainase,
			String nama, String alamat, String lat, String lon, String isProcessed){
		this._tipeAirMinum = tipeAirMinum;
		this._tipeAirLimbah = tipeAirLimbah;
		this._tipePersampahan = tipePersampahan;
		this._tipeDrainase = tipeDrainase;
		this._imgPathAirMinum = imgPathAirMinum;
		this._imgPathAirLimbah = imgPathAirLimbah;
		this._imgPathPersampahan = imgPathPersampahan;
		this._imgPathDrainase = imgPathDrainase;
		//this._kategori = kategori;
		//this._created_at = created_at;
		this._nama = nama;
		this._alamat = alamat;
		this._lat = lat;
		this._lon = lon;
		this._isProcessed = isProcessed;
	}
	
	//getter
	public int getID(){
		return this._id;
	}
	
	public String getTipeAirMinum(){
		return this._tipeAirMinum;
	}
	
	public String getTipeAirLimbah(){
		return this._tipeAirLimbah;
	}
	
	public String getTipePersampahan(){
		return this._tipePersampahan;
	}
	
	public String getTipeDrainase(){
		return this._tipeDrainase;
	}
	
	public String getImgPathAirMinum(){
		return this._imgPathAirMinum;
	}
	
	public String getImgPathAirLimbah(){
		return this._imgPathAirLimbah;
	}
	
	public String getImgPathPersampahan(){
		return this._imgPathPersampahan;
	}
	
	public String getImgPathDrainase(){
		return this._imgPathDrainase;
	}
	
	public String getKategori(){
		return this._kategori;
	}
	
	public String getNama(){
		return this._nama;
	}
	
	public String getAlamat(){
		return this._alamat;
	}

	public String getLat(){
		return this._lat;
	}

	public String getLon(){
		return this._lon;
	}
	
	public String getIsProcessed(){
		return this._isProcessed;
	}

	
	//setter
	public void setID(int id){
		this._id = id;
	}
	
	public void setTipeAirMinum(String tipeAirMinum){
		this._tipeAirMinum = tipeAirMinum;
	}
	
	public void setTipeAirLimbah(String tipeAirLimbah){
		this._tipeAirLimbah = tipeAirLimbah;
	}
	
	public void setTipePersampahan(String tipePersampahan){
		this._tipePersampahan = tipePersampahan;
	}
	
	public void setTipeDrainase(String tipeDrainase){
		this._tipeDrainase = tipeDrainase;
	}

		//image path
	public void setImgPathAirMinum(String imgPathAirMinum){
		this._imgPathAirMinum = imgPathAirMinum;
	}
	
	public void setImgPathAirLimbah(String imgPathAirLimbah){
		this._imgPathAirLimbah = imgPathAirLimbah;
	}
	
	public void setImgPathPersampahan(String imgPathPersampahan){
		this._imgPathPersampahan = imgPathPersampahan;
	}
	
	public void setImgPathDrainase(String imgPathDrainase){
		this._imgPathDrainase =imgPathDrainase;
	}
	
	public void setKategori(String kategori){
		this._kategori = kategori;
	}
	
	public void setCreatedAt(String created_at){
		this._created_at = created_at;
	}
	
	public void setNama(String nama){
		this._nama = nama;
	}
	
	public void setAlamat(String alamat){
		this._alamat = alamat;
	}
	
	public void setLat(String lat){
		this._lat = lat;
	}
	
	public void setLon(String lon){
		this._lon = lon;
	}
	
	public void setIsProcessed(String isProcessed){
		this._isProcessed = isProcessed;
	}
	
}
