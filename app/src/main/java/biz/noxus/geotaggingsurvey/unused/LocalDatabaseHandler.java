package biz.noxus.geotaggingsurvey.unused;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import biz.noxus.geotaggingsurvey.helper.DataPacket;

public class LocalDatabaseHandler extends SQLiteOpenHelper {
	//All Static variables

	private static final int DATABASE_VERSION = 1;// Database Version
	private static final String DATABASE_NAME = "cimancis_db"; // Database Name
	private static final String TABLE_NAME = "package";// Package table name

	// Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_TYPE1 = "tipeAirMinum";
	private static final String KEY_TYPE2 = "tipeAirLimbah";
	private static final String KEY_TYPE3 = "tipePersampahan";
	private static final String KEY_TYPE4 = "tipeDrainase";
	private static final String KEY_IMAGE_PATH1 = "URI_GambarAirMinum";
	private static final String KEY_IMAGE_PATH2 = "URI_GambarAirLimbah";
	private static final String KEY_IMAGE_PATH3 = "URI_GambarPersampahan";
	private static final String KEY_IMAGE_PATH4 = "URI_GambarDrainase";
	private static final String KEY_LAT = "lokasiLat";
	private static final String KEY_LON = "lokasiLong";
	private static final String KEY_NAME = "namaRumahTangga";
	private static final String KEY_ADDRESS = "alamatRumahTangga";
	private static final String KEY_CAT = "kategori";
	private static final String KEY_PROCESSED = "isProcessed";

	public LocalDatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_PACKAGES_TABLE = "CREATE TABLE " + TABLE_NAME + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," 
				+ KEY_TYPE1 + " TEXT,"+ KEY_TYPE2 + " TEXT," + KEY_TYPE3 + " TEXT," + KEY_TYPE4 + " TEXT,"
				+ KEY_IMAGE_PATH1 + " TEXT,"+ KEY_IMAGE_PATH2 + " TEXT," + KEY_IMAGE_PATH3 + " TEXT," + KEY_IMAGE_PATH4 + " TEXT,"
				+ KEY_NAME + " TEXT," + KEY_ADDRESS + " TEXT,"+ KEY_LAT + " TEXT,"+ KEY_LON + " TEXT," 
				+ KEY_PROCESSED +" TEXT)";
		Log.d("dbhandling: ", CREATE_PACKAGES_TABLE);
        try {
            db.execSQL(CREATE_PACKAGES_TABLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

		// Create tables again
		onCreate(db);
	}
	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new data
	public void addPackage(DataPacket paket) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TYPE1, paket.getTipeAirMinum());
		values.put(KEY_TYPE2, paket.getTipeAirLimbah());
		values.put(KEY_TYPE3, paket.getTipePersampahan());
		values.put(KEY_TYPE4, paket.getTipeDrainase());
		values.put(KEY_IMAGE_PATH1, paket.getImgPathAirMinum());
		values.put(KEY_IMAGE_PATH2, paket.getImgPathAirLimbah());
		values.put(KEY_IMAGE_PATH3, paket.getImgPathPersampahan());
		values.put(KEY_IMAGE_PATH4, paket.getImgPathDrainase());
		values.put(KEY_NAME, paket.getNama());
		values.put(KEY_ADDRESS, paket.getAlamat());
		values.put(KEY_LAT, paket.getLat());
		values.put(KEY_LON, paket.getLon());
		values.put(KEY_PROCESSED, paket.getIsProcessed());
		//values.put(KEY_PROCESSED, paket.getAlamat());
		//values.put(KEY_CAT, paket.getKategori());

		// Inserting Row
		db.insert(TABLE_NAME, null, values);
		db.close(); // Closing database connection
	}

	// get single data
	DataPacket getPaket(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_NAME, new String[] { KEY_ID,
				KEY_TYPE1, KEY_CAT }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		DataPacket paket = new DataPacket(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), null, null, null, null,null);

		// return paket
				return paket;
	}

	// get all data
	public List<DataPacket> getAllPackage() {
		List<DataPacket> packageList = new ArrayList<DataPacket>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_NAME;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				DataPacket paket = new DataPacket();
				paket.setID(Integer.parseInt(cursor.getString(0)));
				paket.setTipeAirMinum(cursor.getString(1));
				paket.setTipeAirLimbah(cursor.getString(2));
				paket.setTipePersampahan(cursor.getString(3));
				paket.setTipeDrainase(cursor.getString(4));
				paket.setImgPathAirMinum(cursor.getString(5));
				paket.setImgPathAirLimbah(cursor.getString(6));
				paket.setImgPathPersampahan(cursor.getString(7));
				paket.setImgPathDrainase(cursor.getString(8));
				paket.setNama(cursor.getString(9));
				paket.setAlamat(cursor.getString(10));
				paket.setLat(cursor.getString(11));
				paket.setLon(cursor.getString(12));
				paket.setIsProcessed(cursor.getString(13));
				// Adding packets to list
				packageList.add(paket);
			} while (cursor.moveToNext());
		}

		// return contact list
		//db.close();//close database
		return packageList;
	}

	// Updating single contact
	public int updatePackage(DataPacket paket) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TYPE1, paket.getTipeAirMinum());
		values.put(KEY_CAT, paket.getKategori());

		// updating row
		return db.update(TABLE_NAME, values, KEY_ID + " = ?",
				new String[] { String.valueOf(paket.getID()) });
	}

	// Deleting all packet
	public void deletePacket() {
		SQLiteDatabase db = this.getWritableDatabase();
		//db.delete(TABLE_NAME, KEY_ID + " = ?",
		//      new String[] { String.valueOf(paket.getID()) });
		db.delete(TABLE_NAME, null, null);
		db.close();
	}


	// Getting Package Count
	public int getPackageCount() {
		String countQuery = "SELECT  * FROM " + TABLE_NAME;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

}
