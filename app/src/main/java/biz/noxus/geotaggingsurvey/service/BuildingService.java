package biz.noxus.geotaggingsurvey.service;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Cache;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.List;
import java.util.Map;

import biz.noxus.geotaggingsurvey.model.Building;

public class BuildingService {
    private static BuildingService instanceOfBuildingService = null;
    private static Building building = new Building();
    public String listOfAllBuildingStoredLocally;
    public static String listOfTheFieldsToUpdate;
    public static Integer numberOfUsedParamIndex;

    public static BuildingService getInstance() {
        if (instanceOfBuildingService == null) {
            instanceOfBuildingService = new BuildingService();
        }
        return instanceOfBuildingService;
    }

    public void addMoreBuilding(Map fromFragment) {
        building = new Building();
        building.buildingType = ((String) fromFragment.get("tipeBangunan"));
        building.geoObjectGroupId = ((Integer) fromFragment.get("groupObjectId"));
        building.fotoProgressBeginning = ((String) fromFragment.get("fotoProgress1"));
        building.fotoProgressMiddle = ((String) fromFragment.get("fotoProgress2"));
        building.fotoProgressEnd = ((String) fromFragment.get("fotoProgress3"));
        building.isUploaded = false;
        building.pelaksana = (String) fromFragment.get("pelaksana");
        building.save();
    }

    public void updateBuildingData(Map fromFragment) {
        building.buildingType = ((String) fromFragment.get("tipeBangunan"));
        building.geoObjectGroupId = ((Integer) fromFragment.get("groupObjectId"));
        building.fotoProgressBeginning = ((String) fromFragment.get("fotoProgress1"));
        building.fotoProgressMiddle = ((String) fromFragment.get("fotoProgress2"));
        building.fotoProgressEnd = ((String) fromFragment.get("fotoProgress3"));
        building.isUploaded = false;
        building.save();
    }

    public void setBuildingUploaded() {
        building.isUploaded = true;
        building.save();
    }

    public void getAllBuilding() {
        Select select = new Select();
        List<Building> buildings = select.all().from(Building.class).execute();

        StringBuilder listOfBuildingsInDb = new StringBuilder();
        for (Building building : buildings) {
            listOfBuildingsInDb
                    .append(building.getId())
                    .append("+")
                    .append(building.buildingType)
                    .append("+")
                    .append(building.geoObjectGroupId)
                    .append("+")
                    .append(building.fotoProgressBeginning)
                    .append("+")
                    .append(building.fotoProgressMiddle)
                    .append("+")
                    .append(building.fotoProgressEnd)
                    .append("+")
                    .append(building.pelaksana)
                    .append("+")
                    .append(building.isUploaded)
                    .append("~");
        }
        Log.i("all saved buildings", listOfBuildingsInDb.toString());
        this.listOfAllBuildingStoredLocally = listOfBuildingsInDb.toString();

    }

    public String listOfBuildingToUpload(){
        StringBuilder sb = new StringBuilder();
        sb.append(building.getId())
                .append(building.getId())
                .append("+")
                .append(building.buildingType)
                .append("+")
                .append(building.geoObjectGroupId)
                .append("+")
                .append(building.fotoProgressBeginning)
                .append("+")
                .append(building.fotoProgressMiddle)
                .append("+")
                .append(building.fotoProgressEnd)
                .append("+")
                .append(building.pelaksana)
                .append("+")
                .append(building.isUploaded)
                .append("~");

        return sb.toString();

    }


    public void getOneBuildingToUpload(List<Building> oneBuilding) {


    }

    public Long getCurrentBuildingId() {
        return building.getId();
    }

    public static Cursor fetchResultCursor(String thrownFilter) {
        String tableName = Cache.getTableInfo(Building.class).getTableName();
        String resultRecord = new Select(tableName + ".*, " + tableName + ".Id as _id").from(Building.class).where(thrownFilter).toSql();
        Log.d("query result", resultRecord);

        return Cache.openDatabase().rawQuery(resultRecord, null);
    }

    public static List<Building> getOneBuilding(String fetchedId) {
        return new Select().from(Building.class)
                .where("id = ?", fetchedId)
                .execute();
    }

    public void insertIdfromGeoObject(Building building) {

    }

    public void setBuildingModelInstanceFromSelectedId(String buildingId) {
        building = new Select().from(Building.class)
                .where("id = ?", buildingId)
                .executeSingle();
    }

    public void bulkSavingMultipleFieldsToMutipleColumnInTable(SharedPreferences thrownSharedPrefs) {
        String prefix = "param";
        char quotes = '"';
        String fieldToModify, contentToModify;
        Integer repeat = thrownSharedPrefs.getInt("textEditTotalCount", 0);
        numberOfUsedParamIndex = repeat;

        Log.d("thrown id is", thrownSharedPrefs.getString("setThrownIdForDBSearching",null));
        Log.d("dbsave building id is", building.getId().toString());
        Log.d("dbsave how many repeats", repeat.toString());

        ActiveAndroid.beginTransaction();

        try {
            for (int i = 1; i <= repeat; i++) {
                contentToModify = quotes + thrownSharedPrefs.getString(prefix + i, "-") + quotes;

                fieldToModify = quotes + prefix + i + quotes;

                new Update(Building.class)
                        .set(fieldToModify + " = " + contentToModify)
                        .where("id = ?", building.getId())
                        .execute();
                Log.d("dbsave id", building.getId().toString());
                Log.d("dbsave field", fieldToModify);
                Log.d("dbsave field content", contentToModify);

            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static void bulkUploadMultipleFields(Cursor thrownCursor) {
        String prefix = "param";
        char quotes = '"';
        String fieldsThatProvideTheContent, contentOfField;

        Integer repeat = numberOfUsedParamIndex;

        StringBuilder theListOfTheFieldsToUpdate = new StringBuilder();

        Log.d("dbload repeats", repeat.toString());
        for (int i = 1; i <= repeat; i++) {

            fieldsThatProvideTheContent = prefix + i;
            contentOfField = thrownCursor.getString(thrownCursor.getColumnIndex(fieldsThatProvideTheContent));

            theListOfTheFieldsToUpdate.append(building.geoObjectGroupId)
                    .append("+")
                    .append(fieldsThatProvideTheContent)
                    .append("+")
                    .append(contentOfField)
                    .append("~");

            Log.d("dbload field ", fieldsThatProvideTheContent);
//            Log.d("dbload field content ", contentOfField);
            Log.d("resulting string", theListOfTheFieldsToUpdate.toString());
        }

        listOfTheFieldsToUpdate = theListOfTheFieldsToUpdate.toString();
        Log.d("will upload string", theListOfTheFieldsToUpdate.toString());

    }

}
