package biz.noxus.geotaggingsurvey.service;

import android.database.Cursor;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Cache;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.List;
import java.util.Map;

import biz.noxus.geotaggingsurvey.model.Asset;

/**
 * Created by Rangga on 11/18/2015.
 */
public class AssetService {
    private static AssetService instanceOfAssetService = null;
    private static Asset asset = new Asset();
    public String listOfAllAssetStoredLocally;
    public static Integer numberOfUsedParamIndexForAsset;
    public static String listOfTheAssetFieldsToUpdate;


    public static AssetService getInstance() {
        if (instanceOfAssetService == null) {
            instanceOfAssetService = new AssetService();
        }
        return instanceOfAssetService;
    }

    public static void addAssetCoordinates(Map fromFragment) {
        asset.assetLatitude = (String) fromFragment.get("assetLat");
        asset.assetLongitude = (String) fromFragment.get("assetLong");
        asset.assetElevation = (String) fromFragment.get("assetElev");
        asset.isUploaded = false;
        asset.save();
    }

    public static void addAssetPhoto(String fromFragment) {
        asset.assetPhoto = fromFragment;
        asset.isUploaded = false;
        asset.save();
    }

    public static void addPelaksana(String pelaksana) {
        asset.pelaksana = pelaksana;
        asset.isUploaded = false;
        asset.save();
    }

    public static void addAssetTypeAndSubAssetType(Map fromFragment) {
        asset.assetType = (String) fromFragment.get("tipeAsset");
        asset.subAssetType = (String) fromFragment.get("tipeSubAsset");
        asset.isUploaded = false;
        asset.save();
    }

    public static void setUploaded() {
        asset.isUploaded = true;
        asset.save();
    }

    public Long getCurrentAssetId() {
        return asset.getId();
    }

    public static List<Asset> getOneAsset(String fetchedId) {
        return new Select().from(Asset.class)
                .where("id = ?", fetchedId)
                .execute();
    }

    public void setAssetModelInstanceFromSelectedId(String assetId){
        asset = new Select().from(Asset.class)
                .where("id = ?", assetId)
                .executeSingle();
    }

    public void getAllAsset() {
        Select select = new Select();
        List<Asset> assets = select.all().from(Asset.class).execute();

        StringBuilder listOfAssetsInDB = new StringBuilder();
        for (Asset asset : assets) {
            listOfAssetsInDB
                    .append(asset.getId())
                    .append("+")
                    .append(asset.assetType)
                    .append("+")
                    .append(asset.subAssetType)
                    .append("+")
                    .append(asset.assetLatitude)
                    .append("+")
                    .append(asset.assetLongitude)
                    .append("+")
                    .append(asset.assetPhoto)
                    .append("+")
                    .append(asset.pelaksana)
                    .append("~");
        }
        Log.i("all saved buildings", listOfAssetsInDB.toString());
        this.listOfAllAssetStoredLocally = listOfAssetsInDB.toString();

    }

    public static Cursor fetchResultCursor(String thrownFilter) {

        String tableName = Cache.getTableInfo(Asset.class).getTableName();

        String resultRecord = new Select(tableName + ".*, " + tableName + ".Id as _id").from(Asset.class).where(thrownFilter).toSql();
        Log.d("this is the resulting query", resultRecord);
        return Cache.openDatabase().rawQuery(resultRecord, null);
    }

    public static void bulkSaveMultipleFieldsToMutipleColumnInTable(Map inputtedHash) {
        String prefix = "param";
        char quotes = '"';
        String fieldToModify, contentToModify;
        Integer repeat = (Integer) inputtedHash.get("textEditTotalCount");

        numberOfUsedParamIndexForAsset = repeat;

        ActiveAndroid.beginTransaction();

        try {
            for (int i = 1; i <= repeat; i++) {
                contentToModify = quotes + (String) inputtedHash.get(prefix + i) + quotes;

                fieldToModify = quotes + prefix + i + quotes;

                new Update(Asset.class)
                        .set(fieldToModify + " = " + contentToModify)
                        .where("id = ?", asset.getId())
                        .execute();
                Log.d("thrown table string", fieldToModify);
                Log.d("thrown content string", contentToModify);
                Log.d("how many repeats", repeat.toString());
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static void bulkUploadMultipleFields(Cursor thrownCursor) {
        String prefix = "param";
        char quotes = '"';
        String fieldsThatProvideTheContent, contentOfField;

        Integer repeat = numberOfUsedParamIndexForAsset;


        StringBuilder theListOfTheFieldsToUpdate = new StringBuilder();


        for (int i = 1; i <= repeat; i++) {

            fieldsThatProvideTheContent = prefix + i;
            contentOfField = thrownCursor.getString(thrownCursor.getColumnIndex(fieldsThatProvideTheContent));

            theListOfTheFieldsToUpdate.append(asset.getId())
                    .append("+")
                    .append(fieldsThatProvideTheContent)
                    .append("+")
                    .append(contentOfField)
                    .append("~");

            Log.d("thrown table string", fieldsThatProvideTheContent);
//            Log.d("thrown content string", contentOfField);
            Log.d("how many repeats", repeat.toString());
            Log.d("resulting string", theListOfTheFieldsToUpdate.toString());
        }

        listOfTheAssetFieldsToUpdate = theListOfTheFieldsToUpdate.toString();
        Log.d("will upload string", theListOfTheFieldsToUpdate.toString());

    }
}
