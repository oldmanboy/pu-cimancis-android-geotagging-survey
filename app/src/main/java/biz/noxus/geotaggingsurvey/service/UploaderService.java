package biz.noxus.geotaggingsurvey.service;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rangga on 11/18/2015.
 */
public class UploaderService {

    public String POST_URL;
    public  String stringToSend;
    private Context mcontext;

    public UploaderService(Context context){
        this.mcontext = context;
    }


    public void uploader(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, POST_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.trim().equals("success")) {
                            //do something
                            Toast.makeText(mcontext, response.toString(), Toast.LENGTH_LONG).show();
                        } else {

                            Toast.makeText(mcontext,"fail",Toast.LENGTH_LONG).show();
                            Toast.makeText(mcontext,response.toString(),Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        // get the error and show it
                        Toast.makeText(mcontext,error.toString(),Toast.LENGTH_LONG).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("raw", stringToSend);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        requestQueue.add(stringRequest);
    }



}
