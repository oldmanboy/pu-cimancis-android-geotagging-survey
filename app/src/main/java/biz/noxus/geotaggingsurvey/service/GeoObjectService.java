package biz.noxus.geotaggingsurvey.service;

import android.util.Log;

import com.activeandroid.query.Select;

import java.util.List;
import java.util.Map;

import biz.noxus.geotaggingsurvey.model.GeoObject;


public class GeoObjectService {
    private static GeoObjectService instanceOfGeoObjectService = null;
    private GeoObject geoObject = new GeoObject();
    public String listOfAllGeoObjectStoredLocally = "";

    public static GeoObjectService getInstance(){
        if (instanceOfGeoObjectService == null) {
            instanceOfGeoObjectService = new GeoObjectService();
        }
        return instanceOfGeoObjectService;
    }

    public void addIndividualGeoObjectComponents(Map fromFragment) {

        geoObject.setGroupObjectId((Integer) fromFragment.get("groupObjectId"));
        geoObject.setGroupObjectNo((Integer) fromFragment.get("groupObjectNo"));
        geoObject.setKoordinatElevasi((String) fromFragment.get("groupElev"));
        geoObject.setKoordinatLatitude((String) fromFragment.get("groupLat"));
        geoObject.setKoordinatLongitude((String) fromFragment.get("groupLong"));
        geoObject.pelaksana = (String) fromFragment.get("pelaksana");
        geoObject.setIsUploaded(false);
        geoObject.save();
    }

    public void addGeoObject() {

    }

    public void deleteGeoObject() {

    }

    public void getAllGeoObject() {
        Select select = new Select();
        List<GeoObject> geoObjects = select.all().from(GeoObject.class).execute();

        StringBuilder listOfGeoObjectInDb = new StringBuilder();
        for(GeoObject geoObject : geoObjects){
            listOfGeoObjectInDb
                    .append(geoObject.groupObjectId)
                    .append(":")
                    .append(geoObject.groupObjectNo)
                    .append(":")
                    .append(geoObject.koordinatLatitude)
                    .append(":")
                    .append(geoObject.koordinatLongitude)
                    .append(":")
                    .append(geoObject.pelaksana)
                    .append(":")
                    .append(geoObject.koordinatElevasi)
                    .append("/")
                    ;

        }
        Log.v("all saved geo object", listOfGeoObjectInDb.toString());
        this.listOfAllGeoObjectStoredLocally = listOfGeoObjectInDb.toString();
    }

    public static List<GeoObject> fetchResult(int fetchedId){
        return new Select().from(GeoObject.class)
                .where("group_object_id = ?", fetchedId)
                .execute();
    }

    public void setUploaded(){

    }

    public void getOneSetOfGeoObject(){

    }

    public String listOfGeoObjectToUpload(){
        StringBuilder sb = new StringBuilder();
        sb.append(geoObject.groupObjectId)
                .append(":")
                .append(geoObject.groupObjectNo)
                .append(":")
                .append(geoObject.koordinatLatitude)
                .append(":")
                .append(geoObject.koordinatLongitude)
                .append(":")
                .append(geoObject.pelaksana)
                .append(":")
                .append(geoObject.koordinatElevasi)
                .append("/");

        return sb.toString();
    }
}

