package biz.noxus.geotaggingsurvey.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import biz.noxus.geotaggingsurvey.R;

/**
 * Created by Rangga on 11/19/2015.
 */
public class AssetAdapter extends CursorAdapter {


    public AssetAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.activity_data_view_individual_items,parent,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView labelOfDataItem = (TextView) view.findViewById(R.id.labelOfdataItem);
        TextView itemNumber = (TextView) view.findViewById(R.id.numberItem);

        int position = cursor.getPosition() + 1;
        String assetType = cursor.getString(cursor.getColumnIndexOrThrow("asset_type"));

        itemNumber.setText(String.valueOf(position));
        labelOfDataItem.setText("Asset : " + assetType);
    }
}
