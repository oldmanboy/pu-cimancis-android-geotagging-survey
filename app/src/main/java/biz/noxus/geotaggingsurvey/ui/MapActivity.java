package biz.noxus.geotaggingsurvey.ui;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import biz.noxus.geotaggingsurvey.R;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;


public class MapActivity extends FragmentActivity implements AdapterView.OnItemClickListener {

    GoogleMap googleMap;
    GoogleMapOptions gMapOptions;
    ListView drawerList;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerListener;
    private static final LatLng CIREBON = new LatLng(-6.71909, 108.563211);


    /**
     * Initialises the mapview
     */
    private void createMapView() {
        /**
         * Catch the null pointer exception that
         * may be thrown when initialising the map
         */
        try {
            if (null == googleMap) {
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                        R.id.mapView)).getMap();

                /**
                 * If the map is still null after attempted initialisation,
                 * show an error to the user
                 */
                if (null == googleMap) {
                    Toast.makeText(getApplicationContext(),
                            "Error creating map", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (NullPointerException exception) {
            Log.e("mapApp", exception.toString());
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);

        //add the drawer menu list and behaviors
        drawerList = (ListView) findViewById(R.id.drawerList);
//        menuAdapter = new MenuAdapter(this);
//        drawerList.setAdapter(menuAdapter);
        drawerList.setOnItemClickListener(this);

        //add and set drawer behaviour
        //set the drawer icon to be clickable
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        drawerListener = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.drawable.ic_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerListener);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        createMapView();

        // Move the camera instantly to Cirebon with a zoom of 15.
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(CIREBON, 15));
        // set default map settings after view is initialized
        googleMap.setMyLocationEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);



        // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(CIREBON)      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        //  googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerListener.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerListener.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard, menu);
        return true;
    }


    //add the menu list action set
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                Intent i0 = new Intent(this, SurveyorActivity.class);
                startActivity(i0);
                finish();
                break;
            case 1:
                PrefUtils.resetPref(this);
                Intent i1 = new Intent(this, PAIActivity.class);
                startActivity(i1);
                //finish();
                break;
            case 2:
                PrefUtils.resetPref(this);
                Intent i2 = new Intent(this, AssetActivity.class);
                startActivity(i2);
                finish();
                break;
            case 3:
                PrefUtils.resetPref(this);
                Intent i3 = new Intent(this, MapActivity.class);
                startActivity(i3);
                finish();
                break;
            case 4:
                Intent i4 = new Intent(this, DataActivity.class);
                startActivity(i4);
                finish();
                break;
            case 5:
                Intent i5 = new Intent(this, LoginActivity.class);
                startActivity(i5);
                finish();
                break;
        }
    }

//    private void drawpolygon(DistanceInfo  array[]) {
//
//        int length = array.length;
//
//        // Optional length checks. Modify yourself.
////        if(length == 0)
////        {
////            // Do whatever you like then get out. Do not run the following.
////            return;
////        }
//
//        // We have a length of not 0 so...
//        PolygonOptions poly = new PolygonOptions();
//        poly.fillColor(Color.GRAY);
//
//        // Initial point
//        poly.add(CIREBON);
//
//        // ... then the rest.
//        for(int i = 0; i < length; i++)
//        {
//            poly.add(new LatLng(array[i].a, array[i].b));
//        }
//
//        // Done! Add to map.
//        googleMap.addPolygon(poly);
//    }
//
}



