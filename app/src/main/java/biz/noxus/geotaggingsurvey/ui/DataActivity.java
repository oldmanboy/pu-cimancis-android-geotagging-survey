package biz.noxus.geotaggingsurvey.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import biz.noxus.geotaggingsurvey.R;
import biz.noxus.geotaggingsurvey.adapter.AssetAdapter;
import biz.noxus.geotaggingsurvey.adapter.GeoTagAdapter;
import biz.noxus.geotaggingsurvey.service.AssetService;
import biz.noxus.geotaggingsurvey.service.BuildingService;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardView;


public class DataActivity extends Activity implements AdapterView.OnItemClickListener {
    ListView drawerList, dataList, assetDataList;
    TextView emptyLabel;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_view);
        //add the drawer menu list and behaviors
        drawerList = (ListView) findViewById(R.id.drawerList);
//        menuAdapter = new MenuAdapter(this);
//        drawerList.setAdapter(menuAdapter);
        drawerList.setOnItemClickListener(this);

        //add and set drawer behaviour
        //set the drawer icon to be clickable
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        drawerListener = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.drawable.ic_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerListener);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        // Prepare 2 Cards
        Card card1 = new Card(this, R.layout.activity_data_view_progress);
        Card card2 = new Card(this, R.layout.activity_data_view_assets);

        // Create a CardHeader
        CardHeader header = new CardHeader(this);
        header.setTitle("Hello world");

        // Set the cards in the cardView
        CardView cardView = (CardView) findViewById(R.id.pendingDataView);
        cardView.setCard(card1);
        CardView cardView2 = (CardView) findViewById(R.id.doneDataView);
        cardView2.setCard(card2);

        // Get the list
        dataList = (ListView) findViewById(R.id.listOfBuildingDataView);
        assetDataList = (ListView) findViewById(R.id.listOfAssetDataView);

        emptyLabel = (TextView) findViewById(R.id.labelKetersediaanDaftarDataProgress);
//        dataList.setEmptyView(emptyLabel);
//load the list
        BuildingService buildingService = new BuildingService();
        AssetService assetService = new AssetService();
        //filter of query
        final Cursor GeoCursor = buildingService.fetchResultCursor("");// this is the real filter uploaded_state = '0'
        final Cursor AssetCursor = assetService.fetchResultCursor("");

        final GeoTagAdapter geoTagAdapter = new GeoTagAdapter(this, GeoCursor, 0);
        final AssetAdapter assetAdapter = new AssetAdapter(this, AssetCursor, 0);


//        final GeoTagAdapter adapter = new GeoTagAdapter(this,myList);
        dataList.setAdapter(geoTagAdapter);
        dataList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                GeoCursor.moveToPosition(position);

//                Toast.makeText(getApplicationContext(),
//                        "yo ", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getApplicationContext(), SurveyorActivity.class);

                Bundle b = new Bundle();
                b.putString("itemFromDatabaseIndex", GeoCursor.getString(GeoCursor.getColumnIndex("_id")));
                i.putExtras(b);
                startActivity(i);
//                finish();
            }
        });


        assetDataList.setAdapter(assetAdapter);
        assetDataList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AssetCursor.moveToPosition(position);

//                Toast.makeText(getApplicationContext(),
//                        "yo ", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getApplicationContext(), AssetActivity.class);

                Bundle b = new Bundle();
                b.putString("itemFromDatabaseIndex", AssetCursor.getString(AssetCursor.getColumnIndex("_id")));
                i.putExtras(b);
                startActivity(i);
//                finish();
            }
        });
        //load data from database

//        final ArrayList myList = buildingService.getAllBuildingTestVersion();
//        final ArrayAdapter stableAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, myList);
//        dataList.setAdapter(stableAdapter);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerListener.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerListener.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard, menu);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                Intent i0 = new Intent(this, SurveyorActivity.class);
                startActivity(i0);
                finish();
                break;
            case 1:
                PrefUtils.resetPref(this);
                Intent i1 = new Intent(this, PAIActivity.class);
                startActivity(i1);
                //finish();
                break;
            case 2:
                PrefUtils.resetPref(this);
                Intent i2 = new Intent(this, AssetActivity.class);
                startActivity(i2);
                finish();
                break;
            case 3:
                PrefUtils.resetPref(this);
                Intent i3 = new Intent(this, MapActivity.class);
                startActivity(i3);
                finish();
                break;
            case 4:
                Intent i4 = new Intent(this, DataActivity.class);
                startActivity(i4);
                finish();
                break;
            case 5:
                Intent i5 = new Intent(this, LoginActivity.class);
                startActivity(i5);
                finish();
                break;
        }
    }

}
