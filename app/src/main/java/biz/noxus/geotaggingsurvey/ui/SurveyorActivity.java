package biz.noxus.geotaggingsurvey.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import biz.noxus.geotaggingsurvey.R;
import biz.noxus.geotaggingsurvey.adapter.SmartFragmentStatePagerAdapter;
import biz.noxus.geotaggingsurvey.ui.fragment.BaseDataUtamaFragment;
import biz.noxus.geotaggingsurvey.ui.fragment.BaseFotoFragment;
import biz.noxus.geotaggingsurvey.ui.fragment.BaseKoordinatFragment;
import biz.noxus.geotaggingsurvey.ui.fragment.ProgressDataUtamaFragment;
import biz.noxus.geotaggingsurvey.ui.fragment.ProgressFotoFragment;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;

public class SurveyorActivity extends FragmentActivity implements AdapterView.OnItemClickListener {

    ViewPager viewPager = null;
    ListView drawerList;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerListener;
    private MenuAdapter menuAdapter;
    private SmartFragmentStatePagerAdapter adapterViewPager;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surveyor_view);

        //add the tab views and the fragment manager
        viewPager = (ViewPager) findViewById(R.id.pager);
        FragmentManager fragmentManager = getSupportFragmentManager();

//        FragmentTransaction ft = fragmentManager.beginTransaction();
//        ft.add(new BaseDataUtamaFragment(), "dataUtama");
//        ft.commit();

        viewPager.setAdapter(adapterViewPager = new SurveyorStatePagerAdapter(fragmentManager));

        //add the drawer menu list and behaviors
        drawerList = (ListView) findViewById(R.id.drawerList);
//        menuAdapter = new MenuAdapter(this);
//        drawerList.setAdapter(menuAdapter);
        drawerList.setOnItemClickListener(this);

        //add and set drawer behaviour
        //set the drawer icon to be clickable
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        drawerListener = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.drawable.ic_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerListener);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);



        //retain the state of fragment
        viewPager.setOffscreenPageLimit(3);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //TODO: save the current state from fragment
//                Toast.makeText(getApplicationContext(),"Fragment was changed to " + position + "" + viewPager.getCurrentItem(), Toast.LENGTH_SHORT).show();
                BaseDataUtamaFragment DataUtamaFragment;
                DataUtamaFragment = (BaseDataUtamaFragment) adapterViewPager.getRegisteredFragment(0);
                DataUtamaFragment.saveDataUtamaFragmentInputToPref();
                   }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
//        viewPager.setOffscreenPageLimit(3);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerListener.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerListener.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard, menu);
        return true;
    }

    //add the menu list action set
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                Intent i0 = new Intent(this, SurveyorActivity.class);
                startActivity(i0);
                finish();
                break;
            case 1:
                PrefUtils.resetPref(this);
                Intent i1 = new Intent(this, PAIActivity.class);
                startActivity(i1);
                //finish();
                break;
            case 2:
                PrefUtils.resetPref(this);
                Intent i2 = new Intent(this, AssetActivity.class);
                startActivity(i2);
                finish();
                break;
            case 3:
                PrefUtils.resetPref(this);
                Intent i3 = new Intent(this, MapActivity.class);
                startActivity(i3);
                finish();
                break;
            case 4:
                Intent i4 = new Intent(this, DataActivity.class);
                startActivity(i4);
                finish();
                break;
            case 5:
                Intent i5 = new Intent(this, LoginActivity.class);
                startActivity(i5);
                finish();
                break;
        }
    }


}

class SurveyorStatePagerAdapter extends SmartFragmentStatePagerAdapter {
    private static int NUM_ITEMS = 2;


    public SurveyorStatePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ProgressDataUtamaFragment();
//            case 1:
//                return new BaseKoordinatFragment();
            case 1:
                return new ProgressFotoFragment();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Data Bangunan";
//            case 1:
//                return "Pengambilan Koordinat";
            case 1:
                return "Pengambilan Foto";
            default:
                return null;
        }

    }


}

class MenuAdapter extends BaseAdapter {
    Context con;
    String[] theListOfMenus;


    public MenuAdapter(Context context) {
        theListOfMenus = context.getResources().getStringArray(R.array.sidebarMenuItems);
    }

    @Override
    public int getCount() {
        return theListOfMenus.length;
    }

    @Override
    public Object getItem(int position) {
        return theListOfMenus[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) con
                    .getSystemService(con.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.custom_drawer_row, parent, false);
        } else {
            row = convertView;
        }
        TextView titleDrawerList = (TextView) row.findViewById(R.id.textView1);
        titleDrawerList.setText(theListOfMenus[position]);
        return null;
    }
}