package biz.noxus.geotaggingsurvey.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import biz.noxus.geotaggingsurvey.R;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;

public class LoginActivity extends Activity {

	private static final String LOGIN_URL = "http://202.162.214.236/geotag/app/mobile-login.php";
	private static final String KEY_USERNAME = "username";
	private static final String KEY_PASSWORD = "password";
	private static final String KEY_DEVICEINFO = "deviceInfo";
//	private UserLoginTask mAuthTask = null;

	// Values for username/email and password at the time of the login attempt.
	private String mUser;
	private String mPassword;

	// UI references.
	private EditText mUserView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	private Button mLoginDebugButton;
	private Spinner mLoginCategory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login_view);

		// Set up the login form.
		// mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mUserView = (EditText) findViewById(R.id.user);
		mUserView.setText(mUser);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		// Array grouping of identifier
		ArrayList<String> kategoriLogin = new ArrayList<String>();
		kategoriLogin.add("Konsultan");
		kategoriLogin.add("Surveyor");
//		kategoriLogin.add("Kontraktor");
//		kategoriLogin.add("P4ISDA");

		mLoginCategory = (Spinner) findViewById(R.id.login_category_spinner);
		ArrayAdapter<String> kategoriLoginAdapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, kategoriLogin);
		kategoriLoginAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mLoginCategory.setAdapter(kategoriLoginAdapter);

		// This is For debug button
		mLoginDebugButton = (Button) findViewById(R.id.debug_button);
		mLoginDebugButton.setVisibility(View.INVISIBLE);
		mLoginDebugButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i = new Intent(LoginActivity.this,
						AssetActivity.class);
				startActivity(i);
			}
		});

//		findViewById(R.id.sign_in_button).setOnClickListener(
//				new View.OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						attemptLogin();
//					}
//				});


		findViewById(R.id.login_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						userLogin();
					}
				});



    }


	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// super.onCreateOptionsMenu(menu);
	// getMenuInflater().inflate(R.menu.login, menu);
	// return true;
	// }

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
//		if (mAuthTask != null) {
//			return;
//		}

		// Reset errors.
		mUserView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mUser = mUserView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		loginCheck();

		// Check for a valid email address.
		// if (TextUtils.isEmpty(mEmail)) {
		// mEmailView.setError(getString(R.string.error_field_required));
		// focusView = mEmailView;
		// cancel = true;
		// } else if (!mEmail.contains("@")) {
		// mEmailView.setError(getString(R.string.error_invalid_email));
		// focusView = mEmailView;
		// cancel = true;
		// }

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.

//			mAuthTask = new UserLoginTask();
//			mAuthTask.execute((Void) null);
		}
	}

	// fungsi untuk memeriksa user level dan path dari yang terpakai
	public void loginCheck() {

		if (mUser.equals("konsultan") && mPassword.equals("password")) {
			Intent i = new Intent(LoginActivity.this,
					KonsultanActivity.class);
			//i.putExtra("loginSebagai", "konsultan");
			startActivity(i);
		}

		if (mUser.equals("surveyor") && mPassword.equals("password")) {
			Intent i = new Intent(LoginActivity.this,
					SurveyorActivity.class);
			//i.putExtra("loginSebagai", "surveyor");
			startActivity(i);
		}

		if (mUser.equals("kontraktor") && mPassword.equals("password")) {
			Intent i = new Intent(LoginActivity.this,
					KontraktorActivity.class);
//			i.putExtra("loginSebagai", "kontraktor");
			startActivity(i);
		}

		if (mUser.equals("p4isda") && mPassword.equals("password")) {
			Intent i = new Intent(LoginActivity.this,
					KontraktorActivity.class);
//			i.putExtra("loginSebagai", "p4isda");
			startActivity(i);
		}

		// return true;
	}

	/**
	 * get the login form and process it to the remote server
	 */

	private void userLogin(){


		// Store values at the time of the login attempt.
		mUser = mUserView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		mUser = mUser.trim();
		mPassword = mPassword.trim();

		// Show a progress spinner, and kick off a background task to
		// perform the user login attempt.
		mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
		showProgress(true);


		StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {

						if (response.trim().equals("success")) {
							// TODO: filter response and set user level on device
							//do something
							Intent i = new Intent(LoginActivity.this,
									SurveyorActivity.class);
									PrefUtils.setLoggedIn(LoginActivity.this);
									PrefUtils.setLoggedInUser(LoginActivity.this,mUser);
									startActivity(i);
						} else {
							showProgress(false);
							Toast.makeText(LoginActivity.this,"Login Gagal..",Toast.LENGTH_LONG).show();
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						showProgress(false);
						// get the error and show it
						Toast.makeText(LoginActivity.this,error.toString(),Toast.LENGTH_LONG).show();
					}
				}){
			@Override
		protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> map = new HashMap<String, String>();
				map.put(KEY_USERNAME, mUser);
				map.put(KEY_PASSWORD, mPassword);
				map.put(KEY_DEVICEINFO, Build.BRAND+"-"+Build.MANUFACTURER+"-"+Build.MODEL+"-"+Build.ID);
				return map;
			}
		};

		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}





	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

//	/**
//	 * Represents an asynchronous login/registration task used to authenticate
//	 * the user.
//	 */
//	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
//		@Override
//		protected Boolean doInBackground(Void... params) {
//
//
//			try {
//				// Simulate network access.
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				return false;
//			}
//
////			for (String credential : DUMMY_CREDENTIALS) {
////				String[] pieces = credential.split(":");
////				if (pieces[0].equals(mUser)) {
////					// Account exists, return true if the password matches.
////					return pieces[1].equals(mPassword);
////				}
////			}
//
//
//			return true;
//		}
//
//		@Override
//		protected void onPostExecute(final Boolean success) {
//			mAuthTask = null;
//			showProgress(false);
//
//			if (success) {
//				// finish();
//			} else {
//				mPasswordView
//						.setError(getString(R.string.error_incorrect_password));
//				mPasswordView.requestFocus();
//			}
//		}
//
//		@Override
//		protected void onCancelled() {
//			mAuthTask = null;
//			showProgress(false);
//		}
//	}
}
