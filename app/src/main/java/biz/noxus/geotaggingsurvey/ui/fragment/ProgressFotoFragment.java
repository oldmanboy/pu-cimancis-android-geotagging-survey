package biz.noxus.geotaggingsurvey.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import biz.noxus.geotaggingsurvey.helper.LocationHelper;
import biz.noxus.geotaggingsurvey.model.Building;
import biz.noxus.geotaggingsurvey.model.GeoObject;
import biz.noxus.geotaggingsurvey.service.BuildingService;
import biz.noxus.geotaggingsurvey.service.GeoObjectService;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;

/**
 * Created by ranggapl on 1/25/16.
 */
public class ProgressFotoFragment extends BaseFotoFragment {

    Random randomNumber = new Random();
    List<Building> buildingInQuestionContainer;
    List<GeoObject> theGeoObjectsInQuestion;

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnCapturePicture1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                selectedImagePreviewObject = imgPreview1;
                selectedPlaceholderObject = placeholder1;
                captureImage();
                selectedButtonIndex = 1;
                getAndSetLocation();
            }
        });

        btnCapturePicture2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                selectedImagePreviewObject = imgPreview2;
                selectedPlaceholderObject = placeholder2;
                captureImage();
                selectedButtonIndex = 2;
                getAndSetLocation();
            }
        });

        btnCapturePicture3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                selectedImagePreviewObject = imgPreview3;
                selectedPlaceholderObject = placeholder3;
                captureImage();
                selectedButtonIndex = 3;
                getAndSetLocation();
            }
        });

        if (PrefUtils.getPreviousDataStatus(getActivity()) == true) {
            buildingInQuestionContainer = BuildingService.getOneBuilding(PrefUtils.getThrownIdForDBSearching(getActivity()));
            theGeoObjectsInQuestion = GeoObjectService.fetchResult(buildingInQuestionContainer.get(0).geoObjectGroupId);


            Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                    "Lokasi telah ditambahkan", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
            //restorePreviousDataIntoFragment();
        } else generateGroupObjectId();

    }


    public void getAndSetLocation() {
        LocationWorker locationWorker = new LocationWorker();
        locationWorker.execute();

    }

    public void generateGroupObjectId() {
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("currentGroupObjectId", randomNumber.nextInt(1000));
        editor.commit();
    }


    //TODO move subclass to another class to make the code cleaner
    class LocationWorker extends AsyncTask<Boolean, Integer, Boolean> {

        LocationHelper myLocationHelper = new LocationHelper(getActivity());
        private Integer thisPositionCount;

        @Override
        protected void onPreExecute() {
//            thisProgressBar.setVisibility(View.VISIBLE);
        }


        @Override
        protected Boolean doInBackground(Boolean... params) {

            while (myLocationHelper.gotLocation() == false) {

            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);

//            thisProgressBar.setVisibility(View.INVISIBLE);
//            thisEleResult.setText(((Float) myLocationHelper.getElev()).toString());
//            thisLatResult.setText(((Float) myLocationHelper.getLat()).toString());
//            thisLonResult.setText(((Float) myLocationHelper.getLong()).toString());
//            thisEleResult.setVisibility(View.VISIBLE);
//            thisLatResult.setVisibility(View.VISIBLE);
//            thisLonResult.setVisibility(View.VISIBLE);

            Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                    "Lokasi telah ditambahkan", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();

            //TODO: save to db directly after getting the generated object group id
            //TODO remove to its own method maybe?
            HashMap individualCoordinatSet = new HashMap();
            individualCoordinatSet.put("groupObjectId", sharedPreferences.getInt("currentGroupObjectId", 0));
            individualCoordinatSet.put("groupObjectNo",getThisPositionCount());
            individualCoordinatSet.put("groupElev", String.valueOf(myLocationHelper.getElev()).toString());
            individualCoordinatSet.put("groupLat", String.valueOf(myLocationHelper.getLat()).toString());
            individualCoordinatSet.put("groupLong", String.valueOf(myLocationHelper.getLong()).toString());
            individualCoordinatSet.put("pelaksana",PrefUtils.getLoggedInUser(getActivity().getApplicationContext()));


            toast = Toast.makeText(getActivity().getApplicationContext(),
                    String.valueOf(myLocationHelper.getLat()).toString(), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
            toast = Toast.makeText(getActivity().getApplicationContext(),
                    String.valueOf(myLocationHelper.getLong()).toString(), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();

            new GeoObjectService().addIndividualGeoObjectComponents(individualCoordinatSet);
        }

        public Integer getThisPositionCount() {
            return thisPositionCount;
        }

    }

}
