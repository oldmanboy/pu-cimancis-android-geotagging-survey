package biz.noxus.geotaggingsurvey.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import biz.noxus.geotaggingsurvey.helper.Communicator;
import biz.noxus.geotaggingsurvey.helper.LocationHelper;
import biz.noxus.geotaggingsurvey.model.Asset;
import biz.noxus.geotaggingsurvey.service.AssetService;
import biz.noxus.geotaggingsurvey.service.UploaderService;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;

public class AssetFotoKoordinatFragment extends BaseFotoFragment {

    LocationManager mLocationManager;
    AssetService assetService = AssetService.getInstance();
    Communicator communicator;

    List<Asset> theAssetInQuestion;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicator = (Communicator) getActivity();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnCapturePicture1.setText("Ambil Gambar");
        btnCapturePicture2.setVisibility(View.GONE);
        btnCapturePicture3.setVisibility(View.GONE);
        btnCapturePicture4.setVisibility(View.GONE);

        placeholder2.setVisibility(View.GONE);
        placeholder3.setVisibility(View.GONE);
        placeholder4.setVisibility(View.GONE);


        btnCapturePicture1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                selectedImagePreviewObject = imgPreview1;
                selectedPlaceholderObject = placeholder1;
                captureImage();
                selectedButtonIndex = 1;
                getAndSetLocation();
            }
        });


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doSave();

            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // assetService.setUploaded();
                assetService.getAllAsset();
//                Toast.makeText(getActivity().getApplicationContext(), assetService.listOfAllAssetStoredLocally, Toast.LENGTH_LONG).show();


                //initiate uploader helper class
                UploaderService mruploader = new UploaderService(getActivity().getApplicationContext());
                mruploader.POST_URL = "http://202.162.214.236/geotag/app/remote-add-asset.php";
                mruploader.stringToSend = assetService.listOfAllAssetStoredLocally;
                mruploader.uploader();

                Cursor testCursor = assetService.fetchResultCursor("_id = " + assetService.getCurrentAssetId().toString());
                testCursor.moveToFirst();
                assetService.bulkUploadMultipleFields(testCursor);

                // update kolom sekali banyak
                UploaderService bulkUploadService = new UploaderService(getActivity().getApplicationContext());
                bulkUploadService.POST_URL = "http://202.162.214.236/geotag/app/remote-add-asset-2.php";
                bulkUploadService.stringToSend = assetService.listOfTheAssetFieldsToUpdate;
                bulkUploadService.uploader();

                //upload foto
                try {

                    processURI = (Uri.parse(imgFilePath1).getPath());
//        Toast.makeText(getActivity().getApplicationContext(), processURI, Toast.LENGTH_SHORT).show();

                    Uri thisURI = Uri.parse(processURI);

                    BitmapFactory.Options bmoptions = new BitmapFactory.Options();
                    bmoptions.inSampleSize = 6;
                    Bitmap bitmap = BitmapFactory.decodeFile(processURI, bmoptions);

                    String POST_URL = "http://202.162.214.236/geotag/app/uploader.php";

                    uploadImage(POST_URL, bitmap, Uri.parse(imgFilePath1).getLastPathSegment());

                } catch (Exception e) {

                }


            }
        });


    }

    private final LocationListener locationBot = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(Location location) {
            // Location has changed
            Log.i("locationReport", "Successful location change");
            if (mLocationManager != null) {
                mLocationManager.removeUpdates(locationBot);
            }

            if (location.hasAccuracy()) {
                // mSaveDataGambar.setEnabled(false);

                LAT = String.valueOf(location.getLatitude());
                LON = String.valueOf(location.getLongitude());
                ELEVATION = String.valueOf(location.getAltitude());

                // mLatlonText.setText(String.valueOf(mLastLat)+","+String.valueOf(mLastLon));
//                koordinatLat.setHint(LAT);
//                koordinatLon.setHint(LON);
//                koordinatEle.setHint(ELEVATION);
                // mLoadingBar.setVisibility(View.INVISIBLE);
                // mLocationText.setVisibility(View.VISIBLE);
                // mLatlonText.setVisibility(View.VISIBLE);
                // mTambahLokasiButton.setVisibility(View.VISIBLE);
                // mLokasiTerdekatButton.setVisibility(View.VISIBLE);
//                Toast toast = Toast.makeText(getActivity().getApplicationContext(),
//                        "Lokasi Terkunci", Toast.LENGTH_SHORT);
//                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//                toast.show();
            }
        }
    };

    public void getAndSetLocation() {
        LocationWorker locationWorker = new LocationWorker();
        locationWorker.execute();

    }

    @Override
    public void saveFotoFromFragmentToPref() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("file_uri_1", theAssetInQuestion.get(0).assetPhoto);
        editor.commit();

    }

    @Override
    public void doSave() {
        communicator.initiateAssetTambahDataUtama();

        Toast.makeText(getActivity().getApplicationContext(), "Data Telah Disimpan", Toast.LENGTH_SHORT).show();
        assetService.addAssetPhoto(imgURI1);
        assetService.addPelaksana(PrefUtils.getLoggedInUser(getActivity().getApplicationContext()));

        String loggedInUser = PrefUtils.getLoggedInUser(getActivity().getApplicationContext());
//        Toast.makeText(getActivity().getApplicationContext(), loggedInUser, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void restorePreviousDataToFragment() {
        String fotoProgressBeginning = theAssetInQuestion.get(0).assetPhoto;
//        String fotoProgressMiddle = theBuildingInQuestion.get(0).fotoProgressMiddle;
//        String fotoProgressEnd = theBuildingInQuestion.get(0).fotoProgressEnd;
//
        setPreviewImageFromDB(imgPreview1, placeholder1, fotoProgressBeginning);
//        setPreviewImageFromDB(imgPreview2, placeholder2, fotoProgressMiddle);
//        setPreviewImageFromDB(imgPreview3, placeholder3, fotoProgressEnd);

        try {
            saveFotoFromFragmentToPref();
        } catch (Exception e) {
            Log.e("restoring error because", Log.getStackTraceString(e));
        }
    }

    //TODO move subclass to another class to make the code cleaner
    class LocationWorker extends AsyncTask<Boolean, Integer, Boolean> {

        private TextView thisLatResult, thisLonResult, thisEleResult;
        private Integer thisPositionCount;
        private ProgressBar thisProgressBar;


        LocationHelper myLocationHelper = new LocationHelper(getActivity());

        @Override
        protected void onPreExecute() {
//            thisProgressBar.setVisibility(View.VISIBLE);
        }


        @Override
        protected Boolean doInBackground(Boolean... params) {

            while (myLocationHelper.gotLocation() == false) {

            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);

//            thisProgressBar.setVisibility(View.INVISIBLE);
//            thisEleResult.setText(((Float) myLocationHelper.getElev()).toString());
//            thisLatResult.setText(((Float) myLocationHelper.getLat()).toString());
//            thisLonResult.setText(((Float) myLocationHelper.getLong()).toString());
//            thisEleResult.setVisibility(View.VISIBLE);
//            thisLatResult.setVisibility(View.VISIBLE);
//            thisLonResult.setVisibility(View.VISIBLE);

            Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                    "Lokasi telah ditambahkan", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();

            //TODO: save to db directly after getting the generated object group id
            //TODO remove to its own method maybe?
            HashMap individualCoordinatSet = new HashMap();
            individualCoordinatSet.put("assetElev", String.valueOf(myLocationHelper.getElev()).toString());
            individualCoordinatSet.put("assetLat", String.valueOf(myLocationHelper.getLat()).toString());
            individualCoordinatSet.put("assetLong", String.valueOf(myLocationHelper.getLong()).toString());

            assetService.addAssetCoordinates(individualCoordinatSet);
        }

        public void setThisLatResult(TextView thisLatResult) {
            this.thisLatResult = thisLatResult;
        }

        public void setThisLonResult(TextView thisLonResult) {
            this.thisLonResult = thisLonResult;
        }

        public void setThisEleResult(TextView thisEleResult) {
            this.thisEleResult = thisEleResult;
        }

        public void setThisPositionCount(Integer thisPositionCount) {
            this.thisPositionCount = thisPositionCount;
        }

        public Integer getThisPositionCount() {
            return thisPositionCount;
        }

        public void setThisProgressBar(ProgressBar thisProgressBar) {
            this.thisProgressBar = thisProgressBar;
        }
    }


}
