package biz.noxus.geotaggingsurvey.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import biz.noxus.geotaggingsurvey.R;
import biz.noxus.geotaggingsurvey.helper.BulkDataLoader;
import biz.noxus.geotaggingsurvey.helper.Communicator;
import biz.noxus.geotaggingsurvey.helper.FragmentHelper;
import biz.noxus.geotaggingsurvey.model.Asset;
import biz.noxus.geotaggingsurvey.service.AssetService;
import biz.noxus.geotaggingsurvey.service.BuildingService;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.view.CardView;

public class AssetTambahDataUtamaFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    View inflatedView, selectedAssetView;
    LinearLayout addLayout, addLayoutInsideTheLowerCard;
    LinearLayout extraLayoutContainerAsset;
    Spinner tipeAsset, tipeSubAsset;
    private String title;
    private int page;
    List<Asset> theAssetInQuestion;
    SharedPreferences sharedPreferences;
    CardView kartuBawah;


    int selectedIntResource;

    Integer thrownSubAsset;

    HashMap assetHashMapForSaving = new HashMap();

    //Dibersihkan kemudian...

    HashMap inputtedDataMap = new HashMap();

    Communicator communicator;

    BulkDataLoader bulkDataLoader;

    AssetService assetService = AssetService.getInstance();
    BuildingService buildingService = BuildingService.getInstance();

    Random randomNumber = new Random();
    int generatedRandomId;
    private static int selectedSubAsset = 0, selectedSubAssetViewLayout = 0;

    /*
    View bangunanPengambilan;
    LayoutInflater l = LayoutInflater.from(getActivity().getApplicationContext());

    bangunanPengambilan = l.inflate(R.layout.coordinate_components, null);
    */


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        //TODO: get detail from database to put in the current prefs for user to update
        FragmentHelper.intentStateCheck(getActivity(), getActivity().getIntent());


        inflatedView = inflater.inflate(R.layout.fragment_card_base_asset, container, false);


        extraLayoutContainerAsset = (LinearLayout) inflatedView.findViewById(R.id.extraLayoutContainerForAsset);

        // Create a Card
        Card upperCard = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_bangunan_pilihan_asset);

        CardView upperCardView = (CardView) inflatedView.findViewById(R.id.upperCard);
        upperCardView.setCard(upperCard);

        //set views for the additional cards

        CardView lowerCardView = new CardView(getActivity());

        final Card kartu = new Card(getActivity().getApplicationContext(), R.layout.blank_component);

        extraLayoutContainerAsset.addView(lowerCardView);
        lowerCardView.setId(R.id.temporary_building_input_card_id);
        lowerCardView = (CardView) inflatedView.findViewById(R.id.temporary_building_input_card_id);
        lowerCardView.setCard(kartu);
        lowerCardView.setVisibility(View.INVISIBLE);

        addLayoutInsideTheLowerCard = (LinearLayout) inflatedView.findViewById(R.id.blank_layout);

        kartuBawah = lowerCardView;

        //Generate layout inflater untuk tiap komponen terpilih


//        LayoutInflater assetSelectionInflater = LayoutInflater.from(getActivity());

        tipeAsset = (Spinner) inflatedView.findViewById(R.id.jenisAsset);
        ArrayAdapter tipeBangunanAdapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(), R.array.tipeAssetItems, R.layout.spinner_item);
        tipeBangunanAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        tipeAsset.setAdapter(tipeBangunanAdapter);
        tipeAsset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        setCorrectSubAssetTypeList(R.array.tipeAssetBangunanPengambilan);
                        break;
                    case 2:
                        setCorrectSubAssetTypeList(R.array.tipeAssetBangunanPertemuan);
                        break;
                    case 3:
                        setCorrectSubAssetTypeList(R.array.tipeAssetSaluran);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        selectedAssetView = assetSelectionInflater.inflate(R.layout.fragment_view_bangunan_pilihan_asset,addLayoutInsideTheLowerCard);
//        addLayoutInsideTheLowerCard.addView(selectedAssetView);


        tipeSubAsset = (Spinner) inflatedView.findViewById(R.id.jenisSubAsset);
        tipeSubAsset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


//                if (PrefUtils.getPreviousDataStatus(getActivity()) == true) {
//                    tipeSubAsset.setSelection(selectedSubAsset);
//                }


                kartuBawah.setVisibility(View.VISIBLE);

                LayoutInflater assetSelectionInflater = inflater;

                FragmentManager f = getChildFragmentManager();
                FragmentTransaction transaction = f.beginTransaction();
                Fragment chatFragment = f.findFragmentById(R.id.fragmentPhoto);
                if (chatFragment != null) {
                    transaction.remove(chatFragment);
                    transaction.commitAllowingStateLoss();
                    assetSelectionInflater = assetSelectionInflater.cloneInContext(getActivity());
                }
                addLayoutInsideTheLowerCard.removeAllViews();

                ViewGroup selectedAssetView;
                try {
                    switch (selectedSubAsset) {
                        case R.array.tipeAssetBangunanPengambilan:
                            switch (parent.getSelectedItemPosition()) {
                                case 0:
                                    break;
                                case 1:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pengambilan_bendung, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPengambilanBendung;
                                    break;
                                case 2:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pengambilan_bendungan, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPengambilanBendungan;
                                    break;
                                case 3:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pengambilan_freeintake, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPengambilanFreeIntake;
                                    break;
                                case 4:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pengambilan_outlet, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPengambilanOutlet;
                                    break;
                                case 5:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pengambilan_pompa_elektrik, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPompaElektrik;
                                    break;
                                case 6:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pengambilan_pompa_hidrolik, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPengambilanPompaHidrolik;
                                    break;
                            }
                            break;

                        case R.array.tipeAssetBangunanPertemuan:
                            switch (parent.getSelectedItemPosition()) {
                                case 0:

                                    break;
                                case 1:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pertemuan_bagi, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPertemuanBagi;
                                    break;
                                case 2:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pertemuan_bagisadap, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPertemuanBagiSadap;
                                    break;
                                case 3:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pertemuan_pertemuan, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPertemuanPertemuan;
                                    break;
                                case 4:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pertemuan_sadap, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPertemuanSadap;
                                    break;
                                case 5:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_bangunan_pertemuan_sadaplangsung, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetBangunanPertemuanSadapLangsung;
                                    break;
                            }
                            break;

                        case R.array.tipeAssetSaluran:
                            switch (parent.getSelectedItemPosition()) {
                                case 0:

                                    break;
                                case 1:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_saluran_saluran, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetSaluranSaluran;
                                    break;
                                case 2:
                                    selectedAssetView = (ViewGroup) assetSelectionInflater.inflate(R.layout.fragment_view_asset_saluran_saluran, null);
                                    addLayoutInsideTheLowerCard.addView(selectedAssetView);
                                    selectedIntResource = R.id.fragmentViewAssetSaluranTanggul;
                                    break;
                            }
                            break;
                    }


                } catch (Exception e) {
                    Log.e("this is the error", Log.getStackTraceString(e));

                    try {
                        Log.d("this is the fragment", chatFragment.toString());
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }


                }


                Log.d("the view", extraLayoutContainerAsset.toString());

                Log.d("selected asset is", tipeAsset.getSelectedItem().toString());
                Log.d("selected sub asset is", tipeSubAsset.getSelectedItem().toString());

                inputtedDataMap.put("tipeAsset", tipeAsset.getSelectedItem().toString());
                inputtedDataMap.put("tipeSubAsset", tipeSubAsset.getSelectedItem().toString());

                assetService.addAssetTypeAndSubAssetType(inputtedDataMap);
//                Toast.makeText(getActivity(), position + " Wes selected", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (PrefUtils.getPreviousDataStatus(getActivity()) == true) {
            String temp = assetTypeConverter();
            String[] assetTypes = temp.split("-");

            tipeAsset.setSelection(Integer.parseInt(assetTypes[0]));
            try {
                thrownSubAsset = Integer.parseInt(assetTypes[1]);
//                put a timer so it can be fired after load
                new CountDownTimer(2000,1000){

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        tipeSubAsset.setSelection(thrownSubAsset);
//                        bulkDataLoader.floodEditTextWithWords(inflatedView, selectedIntResource);
                        restoreDataIntoFragment();
                    }
                }.start();

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
//              tipeSubAsset.setSelection(2);

            try {
                Log.d("integet for tipe subasset is", thrownSubAsset.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

// used later
//        if (PrefUtils.getPreviousDataStatus(getActivity()) == true)
//            tipeAsset.setSelection(buildingTypeConverter());

        generatedRandomId = randomNumber.nextInt(1000);

        return inflatedView;
    }


    public String assetTypeConverter() {
        theAssetInQuestion = assetService.getOneAsset(PrefUtils.getThrownIdForDBSearching(getActivity()));

        String assetAndsubAsset = "0-0";

        switch (theAssetInQuestion.get(0).assetType) {
            case "Bangunan Pengambilan":


                switch (theAssetInQuestion.get(0).subAssetType) {

                    case "Bendung":
                        assetAndsubAsset = "1-1";
                        break;
                    case "Bendungan":
                        assetAndsubAsset = "1-2";
                        break;
                    case "Free Intake":
                        assetAndsubAsset = "1-3";
                        break;
                    case "Outlet":
                        assetAndsubAsset = "1-4";
                        break;
                    case "Pompa Elektrik":
                        assetAndsubAsset = "1-5";
                        break;
                    case "Pompa Hidrolik":
                        assetAndsubAsset = "1-6";
                        break;
                }


                break;

            case "Bangunan Pertemuan":


                switch (theAssetInQuestion.get(0).subAssetType) {

                    case "Bagi":
                        assetAndsubAsset = "2-1";
                        break;
                    case "Bagi sadap":
                        assetAndsubAsset = "2-2";
                        break;
                    case "Pertemuan":
                        assetAndsubAsset = "2-3";
                        break;
                    case "Sadap":
                        assetAndsubAsset = "2-4";
                        break;
                    case "Sadap langsung":
                        assetAndsubAsset = "2-5";
                        break;
                }
                break;

            case "Saluran (Alur)":


                switch (theAssetInQuestion.get(0).subAssetType) {

                    case "Saluran":
                        assetAndsubAsset = "3-1";
                        break;
                    case "Tanggul":
                        assetAndsubAsset = "3-2";
                        break;
                }
                break;
        }
        return assetAndsubAsset;
    }

    public void saveDataUtamaFragmentInputToPref() {
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();


        // editor.putString("pekerjaan", pekerjaan.getText().toString());
        // editor.putString("kegiatan", kegiatan.getText().toString());
//        editor.putString("desa", desa.getText().toString());
//        editor.putString("kecamatan", kecamatan.getText().toString());
//        editor.putString("kabkota", kabkota.getText().toString());
//        editor.putString("provinsi", provinsi.getText().toString());


        editor.commit();
    }


    public void setCorrectSubAssetTypeList(int selectedAssetTypeIndex) {
        selectedSubAsset = selectedAssetTypeIndex;
        ArrayAdapter tipeSubAssetAdapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(), selectedAssetTypeIndex, R.layout.spinner_item);
        tipeSubAssetAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        tipeSubAsset.setAdapter(tipeSubAssetAdapter);

    }

    public void processThisFragment() {
        Toast.makeText(getActivity().getApplicationContext(), "Data Sedang diproses ...", Toast.LENGTH_LONG).show();
        BulkDataLoader bulkDataLoader = new BulkDataLoader();

        //auto isi
//        bulkDataLoader.floodEditTextWithWords(inflatedView, selectedIntResource);

        //put into hashmap and save to db
        assetHashMapForSaving = bulkDataLoader.putIntoHashMap(inflatedView, selectedIntResource);
        assetService.bulkSaveMultipleFieldsToMutipleColumnInTable(assetHashMapForSaving);

    }


    public void restoreDataIntoFragment() {

        final Cursor progressCursor = assetService.fetchResultCursor("_id = " + PrefUtils.getThrownIdForDBSearching(getActivity()));

        progressCursor.moveToFirst();

//        Log.d("cursor content testing", String.valueOf(progressCursor.getColumnIndexOrThrow("building_type")));

        bulkDataLoader.loadEditTextWithWordsFromDB(inflatedView, selectedIntResource, progressCursor);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
