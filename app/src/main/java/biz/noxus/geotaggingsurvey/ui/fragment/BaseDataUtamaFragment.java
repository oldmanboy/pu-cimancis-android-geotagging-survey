package biz.noxus.geotaggingsurvey.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.List;
import java.util.Random;

import biz.noxus.geotaggingsurvey.R;
import biz.noxus.geotaggingsurvey.helper.BulkDataLoader;
import biz.noxus.geotaggingsurvey.helper.FragmentHelper;
import biz.noxus.geotaggingsurvey.model.Building;
import biz.noxus.geotaggingsurvey.service.BuildingService;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.view.CardView;

public class BaseDataUtamaFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    View inflatedView;
    LinearLayout extraLayoutContainer;
    Spinner tipeBangunan;
    private String title;
    private int page;
    List<Building> theBuildingInQuestion;
    SharedPreferences sharedPreferences;

    Card kartu;

    BuildingService buildingService = BuildingService.getInstance();
    BulkDataLoader bulkDataLoader = new BulkDataLoader();

    Random randomNumber = new Random();
    int generatedRandomId;
    private static int selectedBuilding = 0;
       /*
    public static BaseDataUtamaFragment newInstance(int page, String title){
        BaseDataUtamaFragment surveyorDataUtamaFragment =  new BaseDataUtamaFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        surveyorDataUtamaFragment.setArguments(args);
        return surveyorDataUtamaFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }
*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //TODO: get detail from database to put in the current prefs for user to update
        FragmentHelper.intentStateCheck(getActivity(), getActivity().getIntent());

        inflatedView = inflater.inflate(R.layout.fragment_card_base, container, false);

        // Create a Card
        Card upperCard = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_bangunan_pilihan_tipe);

        CardView cardView = (CardView) inflatedView.findViewById(R.id.upperCard);
        cardView.setCard(upperCard);

        tipeBangunan = (Spinner) inflatedView.findViewById(R.id.jenisBangunan);
        ArrayAdapter tipeBangunanAdapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(), R.array.tipeBangunanItems, R.layout.spinner_item);
        tipeBangunanAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        tipeBangunan.setAdapter(tipeBangunanAdapter);
        tipeBangunan.setOnItemSelectedListener(this);

        if (PrefUtils.getPreviousDataStatus(getActivity()) == true)
            tipeBangunan.setSelection(buildingTypeConverter());

        generatedRandomId = randomNumber.nextInt(1000);


        return inflatedView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//      Toast.makeText(getActivity(), position + " Was selected", Toast.LENGTH_SHORT).show();


        //get the layout that will be inserted
        extraLayoutContainer = (LinearLayout) inflatedView.findViewById(R.id.extraLayoutContainer);

        //clear the view before loading another
        this.clearTheView();



        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        //tambah new layout apabila terpilih
        CardView cardView = new CardView(getActivity());

        /*
        Berdasarkan dropdownlist dari jenis bangunan ambil layout yang tepat
         */

        switch (position) {
            case 0:
                kartu = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_progress_bangunan_input_stubs);
                extraLayoutContainer.addView(cardView);
                cardView.setId(R.id.temporary_building_input_card_id);
                cardView = (CardView) inflatedView.findViewById(R.id.temporary_building_input_card_id);
                cardView.setCard(kartu);
                cardView.setVisibility(View.INVISIBLE);
                break;
            case 1:
                kartu = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_progress_bangunan_input_stubs);
                extraLayoutContainer.addView(cardView);
                cardView.setId(R.id.temporary_building_input_card_id);
                cardView = (CardView) inflatedView.findViewById(R.id.temporary_building_input_card_id);
                cardView.setCard(kartu);
                editor.putString("tipeBangunan", "Situ");
                editor.commit();
                if (PrefUtils.getPreviousDataStatus(getActivity()) == true) {
                    restorePreviousDataIntoFragment();
//                    Toast.makeText(getActivity(), " Previous data loaded", Toast.LENGTH_SHORT).show();
                }
                break;
            case 2:
                kartu = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_progress_bangunan_input_stubs);
                extraLayoutContainer.addView(cardView);
                cardView.setId(R.id.temporary_building_input_card_id);
                cardView = (CardView) inflatedView.findViewById(R.id.temporary_building_input_card_id);
                cardView.setCard(kartu);
                editor.putString("tipeBangunan", "Sungai");
                editor.commit();
                             if (PrefUtils.getPreviousDataStatus(getActivity()) == true) {
                    restorePreviousDataIntoFragment();
//                    Toast.makeText(getActivity(), " Previous data loaded", Toast.LENGTH_SHORT).show();
                }
                break;
            case 3:
                kartu = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_progress_bangunan_input_stubs);
                extraLayoutContainer.addView(cardView);
                cardView.setId(R.id.temporary_building_input_card_id);
                cardView = (CardView) inflatedView.findViewById(R.id.temporary_building_input_card_id);
                cardView.setCard(kartu);
                editor.putString("tipeBangunan", "Irigasi");
                editor.commit();
                                if (PrefUtils.getPreviousDataStatus(getActivity()) == true) {
                    restorePreviousDataIntoFragment();
//                    Toast.makeText(getActivity(), " Previous data loaded", Toast.LENGTH_SHORT).show();
                }
                break;

            case 4:
                kartu = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_progress_bangunan_input_stubs);
                extraLayoutContainer.addView(cardView);
                cardView.setId(R.id.temporary_building_input_card_id);
                cardView = (CardView) inflatedView.findViewById(R.id.temporary_building_input_card_id);
                cardView.setCard(kartu);
                editor.putString("tipeBangunan", "Bendungan");
                editor.commit();
                             if (PrefUtils.getPreviousDataStatus(getActivity()) == true) {
                    restorePreviousDataIntoFragment();
//                    Toast.makeText(getActivity(), " Previous data loaded", Toast.LENGTH_SHORT).show();
                }
                break;

            case 5:
                kartu = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_progress_bangunan_input_stubs);
                extraLayoutContainer.addView(cardView);
                cardView.setId(R.id.temporary_building_input_card_id);
                cardView = (CardView) inflatedView.findViewById(R.id.temporary_building_input_card_id);
                cardView.setCard(kartu);
                editor.putString("tipeBangunan", "Waduk");
                editor.commit();
                            if (PrefUtils.getPreviousDataStatus(getActivity()) == true) {
                    restorePreviousDataIntoFragment();
//                    Toast.makeText(getActivity(), " Previous data loaded", Toast.LENGTH_SHORT).show();
                }
                break;

        }

//        if (PrefUtils.getPreviousDataStatus(getActivity()) == false)
            // load all variable
//            bulkDataLoader.floodEditTextWithWords(inflatedView, R.id.multipartProgressFragments);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //TODO: get id and choose selection if there is already one

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("Fragment 1 Update", "i just got paused yo!");

        try {
            saveDataUtamaFragmentInputToPref();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void clearTheView() {
        //get the layout that will be inserted
        extraLayoutContainer.removeViewInLayout(inflatedView.findViewById(R.id.temporary_building_input_card_id));
    }

    public void saveDataUtamaFragmentInputToPref() {
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();


       // editor.putString("pekerjaan", pekerjaan.getText().toString());
       // editor.putString("kegiatan", kegiatan.getText().toString());
//        editor.putString("desa", desa.getText().toString());
//        editor.putString("kecamatan", kecamatan.getText().toString());
//        editor.putString("kabkota", kabkota.getText().toString());
//        editor.putString("provinsi", provinsi.getText().toString());

       bulkDataLoader.loadEditTextToPrefEditor(inflatedView,R.id.multipartProgressFragments,editor);

        editor.commit();

       if(extraLayoutContainer == null)Log.i("Fragment 1 Update","myballs is empty" );

        Log.i("Fragment 1 Update","this time the fragment id is"+ generatedRandomId);

    }


    public int buildingTypeConverter() {
        theBuildingInQuestion = buildingService.getOneBuilding(PrefUtils.getThrownIdForDBSearching(getActivity()));

        switch (theBuildingInQuestion.get(0).buildingType) {
            case "Situ":
                return 1;

            case "Sungai":
                return 2;

            case "Irigasi":
                return 3;

            case "Bendungan":
                return 4;

            case "Waduk":
                return 5;

        }
        return 0;
    }


    public void restorePreviousDataIntoFragment() {

        final Cursor progressCursor = buildingService.fetchResultCursor("_id = "+ PrefUtils.getThrownIdForDBSearching(getActivity()));

        progressCursor.moveToFirst();


        Log.d("cursor content testing", String.valueOf(progressCursor.getColumnIndexOrThrow("building_type")));

        bulkDataLoader.loadEditTextWithWordsFromDB(inflatedView,R.id.multipartProgressFragments,progressCursor);
    }

}
