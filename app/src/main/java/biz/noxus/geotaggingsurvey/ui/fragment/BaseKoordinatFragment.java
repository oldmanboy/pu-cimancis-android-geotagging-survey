package biz.noxus.geotaggingsurvey.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import biz.noxus.geotaggingsurvey.R;
import biz.noxus.geotaggingsurvey.helper.LocationHelper;
import biz.noxus.geotaggingsurvey.model.Building;
import biz.noxus.geotaggingsurvey.model.GeoObject;
import biz.noxus.geotaggingsurvey.service.BuildingService;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;
import biz.noxus.geotaggingsurvey.service.GeoObjectService;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.view.CardView;

public class BaseKoordinatFragment extends Fragment {

    View vi;
    Button bUpdateLokasi;
    String LAT, LON, ELEVATION;
    EditText koordinatLat, koordinatLon, koordinatEle;
    LocationManager mLocationManager;
    LinearLayout addLayout, addLayoutInsideThecard;
    Integer coordinatAdderCount = 0;
    Random randomNumber = new Random();
    List<Building> buildingInQuestionContainer;
    List<GeoObject> theGeoObjectsInQuestion;
    CardView finalTambahKartu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        vi = inflater.inflate(R.layout.fragment_card_base, container, false);

        // Create a Card
        Card card = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_koordinat);

        // Create a CardHeader
        //CardHeader header = new CardHeader(this);
        //header.setTitle("Hello world");

        card.setTitle("Card View");
//        CardThumbnail thumb = new CardThumbnail(this);
//        thumb.setDrawableResource(R.drawable.ic_launcher);
//
//        card.addCardThumbnail(thumb);

// Add Header to card
        //card.addCardHeader(header);

        // Set card in the cardView
        CardView cardView = (CardView) vi.findViewById(R.id.upperCard);
        cardView.setCard(card);

        koordinatLat = (EditText) vi.findViewById(R.id.latText);
        koordinatLon = (EditText) vi.findViewById(R.id.lonText);
        koordinatEle = (EditText) vi.findViewById(R.id.eleText);
        bUpdateLokasi = (Button) vi.findViewById(R.id.updateBtn);

        addLayout = (LinearLayout) vi.findViewById(R.id.extraLayoutContainer);

        //tambah new layout apabila terpilih
        CardView tambahKartu = new CardView(getActivity());

        Card kartu = new Card(getActivity().getApplicationContext(), R.layout.blank_component);

        addLayout.addView(tambahKartu);
        tambahKartu.setId(R.id.temporary_building_input_card_id);
        tambahKartu = (CardView) vi.findViewById(R.id.temporary_building_input_card_id);
        tambahKartu.setCard(kartu);
        tambahKartu.setVisibility(View.INVISIBLE);

        addLayoutInsideThecard = (LinearLayout) vi.findViewById(R.id.blank_layout);

        finalTambahKartu = tambahKartu;
        bUpdateLokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddLocationButton(null);

                Log.i("Update Location Button", "tombol update lokasi di click");

            }
        });

//        temporary disable
//        checkLocationManager();


        if (PrefUtils.getPreviousDataStatus(getActivity()) == true) {
            buildingInQuestionContainer = BuildingService.getOneBuilding(PrefUtils.getThrownIdForDBSearching(getActivity()));
            theGeoObjectsInQuestion = GeoObjectService.fetchResult(buildingInQuestionContainer.get(0).geoObjectGroupId);
            restorePreviousDataIntoFragment();
        }
        else generateGroupObjectId();

        return vi;
    }

    private final LocationListener locationBot = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(Location location) {
            // Location has changed
            Log.i("locationReport", "Successful location change");
            if (mLocationManager != null) {
                mLocationManager.removeUpdates(locationBot);
            }

            if (location.hasAccuracy()) {
                // mSaveDataGambar.setEnabled(false);

                LAT = String.valueOf(location.getLatitude());
                LON = String.valueOf(location.getLongitude());
                ELEVATION = String.valueOf(location.getAltitude());

                // mLatlonText.setText(String.valueOf(mLastLat)+","+String.valueOf(mLastLon));
                koordinatLat.setHint(LAT);
                koordinatLon.setHint(LON);
                koordinatEle.setHint(ELEVATION);
                // mLoadingBar.setVisibility(View.INVISIBLE);
                // mLocationText.setVisibility(View.VISIBLE);
                // mLatlonText.setVisibility(View.VISIBLE);
                // mTambahLokasiButton.setVisibility(View.VISIBLE);
                // mLokasiTerdekatButton.setVisibility(View.VISIBLE);
//                Toast toast = Toast.makeText(getActivity().getApplicationContext(),
//                        "Lokasi Terkunci", Toast.LENGTH_SHORT);
//                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//                toast.show();
            }
        }
    };

    public void AddLocationButton(List<GeoObject> previouslySavedGeoObjects) {
        View additionalLocationView;
        LayoutInflater l = LayoutInflater.from(getActivity().getApplicationContext());


        finalTambahKartu.setVisibility(View.VISIBLE);

        additionalLocationView = l.inflate(R.layout.coordinate_components, null);

        final TextView indexAdditionalLocation = (TextView) additionalLocationView.findViewById(R.id.indexOfAdditionalLocation);
        final TextView individualLatitudeText = (TextView) additionalLocationView.findViewById(R.id.addLat);
        final TextView individualLongitudeText = (TextView) additionalLocationView.findViewById(R.id.addLon);
        final TextView individualElevationText = (TextView) additionalLocationView.findViewById(R.id.addEle);
        final ProgressBar findingLocationProgressbar = (ProgressBar) additionalLocationView.findViewById(R.id.coordinateLoading);
        final Button addMoreLocationButton = (Button) additionalLocationView.findViewById(R.id.addMoreLocationBtn);

        if(previouslySavedGeoObjects == null) {

            individualElevationText.setVisibility(View.GONE);
            individualLongitudeText.setVisibility(View.GONE);
            individualLatitudeText.setVisibility(View.GONE);

        }

        else {
            individualElevationText.setText(previouslySavedGeoObjects.get(coordinatAdderCount).koordinatElevasi);
            individualLongitudeText.setText(previouslySavedGeoObjects.get(coordinatAdderCount).koordinatLongitude);
            individualLatitudeText.setText(previouslySavedGeoObjects.get(coordinatAdderCount).koordinatLatitude);
        }

        coordinatAdderCount+=1;
        indexAdditionalLocation.setText("Posisi " + coordinatAdderCount.toString() + " :");
        indexAdditionalLocation.setTextColor(Color.BLACK);
        findingLocationProgressbar.setVisibility(View.GONE);


        individualLongitudeText.setTag("lon" + coordinatAdderCount);

        addMoreLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("Update Location Button", "salah satu tombol di klik");
                //mLocationManager.removeUpdates(locationBot);

                LocationWorker locationWorker = new LocationWorker();
                locationWorker.setThisProgressBar(findingLocationProgressbar);
                locationWorker.setThisPositionCount(coordinatAdderCount);
                locationWorker.setThisLonResult(individualLongitudeText);
                locationWorker.setThisLatResult(individualLatitudeText);
                locationWorker.setThisEleResult(individualElevationText);

                locationWorker.execute();

                Log.i("Id of the button", individualLongitudeText.getTag().toString());

//                SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences("Mydata", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putString("lat", LAT);
//                editor.putString("lon", LON);
//                editor.commit();
//                //get the layout that will be inserted


            }
        });

        addLayoutInsideThecard.addView(additionalLocationView);

    }

    //TODO move subclass to another class to make the code cleaner
    class LocationWorker extends AsyncTask<Boolean, Integer, Boolean> {

        private TextView thisLatResult, thisLonResult, thisEleResult;
        private Integer thisPositionCount;
        private ProgressBar thisProgressBar;


        LocationHelper myLocationHelper = new LocationHelper(getActivity());

        @Override
        protected void onPreExecute() {
            thisProgressBar.setVisibility(View.VISIBLE);
        }


        @Override
        protected Boolean doInBackground(Boolean... params) {

            while (myLocationHelper.gotLocation() == false) {

            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
                        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);

            thisProgressBar.setVisibility(View.INVISIBLE);
            thisEleResult.setText(((Float) myLocationHelper.getElev()).toString());
            thisLatResult.setText(((Float) myLocationHelper.getLat()).toString());
            thisLonResult.setText(((Float) myLocationHelper.getLong()).toString());
            thisEleResult.setVisibility(View.VISIBLE);
            thisLatResult.setVisibility(View.VISIBLE);
            thisLonResult.setVisibility(View.VISIBLE);

            Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                    "Lokasi telah ditambahkan", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();

            //TODO: save to db directly after getting the generated object group id
            //TODO remove to its own method maybe?
            HashMap individualCoordinatSet = new HashMap();
            individualCoordinatSet.put("groupObjectId", sharedPreferences.getInt("currentGroupObjectId", 0));
            individualCoordinatSet.put("groupObjectNo",getThisPositionCount());
            individualCoordinatSet.put("groupElev",String.valueOf(myLocationHelper.getElev()).toString());
            individualCoordinatSet.put("groupLat",String.valueOf(myLocationHelper.getLat()).toString());
            individualCoordinatSet.put("groupLong",String.valueOf(myLocationHelper.getLong()).toString());
            individualCoordinatSet.put("pelaksana",PrefUtils.getLoggedInUser(getActivity().getApplicationContext()));

            new GeoObjectService().addIndividualGeoObjectComponents(individualCoordinatSet);
        }

        public void setThisLatResult(TextView thisLatResult) {
            this.thisLatResult = thisLatResult;
        }

        public void setThisLonResult(TextView thisLonResult) {
            this.thisLonResult = thisLonResult;
        }

        public void setThisEleResult(TextView thisEleResult) {
            this.thisEleResult = thisEleResult;
        }

        public void setThisPositionCount(Integer thisPositionCount) {
            this.thisPositionCount = thisPositionCount;
        }

        public Integer getThisPositionCount() {
            return thisPositionCount;
        }

        public void setThisProgressBar(ProgressBar thisProgressBar) {
            this.thisProgressBar = thisProgressBar;
        }
    }


    public void generateGroupObjectId(){
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("currentGroupObjectId", randomNumber.nextInt(1000));
        editor.commit();
    }

    public void restorePreviousDataIntoFragment() {
        //TODO ambil dari database dan taro hasil list dalam for loop untuk generate button

        for(int i = 0; i <theGeoObjectsInQuestion.size();i++){
            AddLocationButton(theGeoObjectsInQuestion);
//            Toast.makeText(getActivity().getApplicationContext(),
//                    "Restoring process ... "+ i+ " " + theGeoObjectsInQuestion.get(i).koordinatLatitude, Toast.LENGTH_SHORT).show();
        }

    }

    public void setKoordinatFragmentPrefState() {

    }
}

