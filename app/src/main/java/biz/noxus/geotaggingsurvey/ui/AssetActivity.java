package biz.noxus.geotaggingsurvey.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import biz.noxus.geotaggingsurvey.adapter.SmartFragmentStatePagerAdapter;
import biz.noxus.geotaggingsurvey.helper.Communicator;
import biz.noxus.geotaggingsurvey.ui.fragment.AssetTambahDataUtamaFragment;

public class AssetActivity extends SurveyorActivity implements Communicator {

    private SmartFragmentStatePagerAdapter AssetAdapterViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragmentManager = getSupportFragmentManager();


        viewPager.setAdapter(AssetAdapterViewPager = new AssetStatePagerAdapter(fragmentManager));


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }


    @Override
    public void initiateAssetTambahDataUtama() {
        AssetTambahDataUtamaFragment DataUtamaFragment;
        DataUtamaFragment = (AssetTambahDataUtamaFragment) AssetAdapterViewPager.getRegisteredFragment(0);
        DataUtamaFragment.processThisFragment();
    }
}

class AssetStatePagerAdapter extends SmartFragmentStatePagerAdapter {
    private static int NUM_ITEMS = 1;


    public AssetStatePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new AssetTambahDataUtamaFragment();
//            case 1:
//                return new AssetFotoKoordinatFragment();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Data Asset";
//            case 1:
//                return "Foto dan Koordinat";
            default:
                return null;
        }

    }

}

