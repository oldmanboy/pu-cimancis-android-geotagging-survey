package biz.noxus.geotaggingsurvey.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import biz.noxus.geotaggingsurvey.R;
import biz.noxus.geotaggingsurvey.helper.FragmentHelper;
import biz.noxus.geotaggingsurvey.helper.BulkDataLoader;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.view.CardView;

/**
 * Created by Rangga on 4/9/2015.
 */
public class PAIDataUtamaFragment extends BaseDataUtamaFragment {

    Button saveButton, uploadButton;
    BulkDataLoader bulkDataLoader = new BulkDataLoader();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //TODO: get detail from database to put in the current prefs for user to update
        FragmentHelper.intentStateCheck(getActivity(), getActivity().getIntent());

//        if (PrefUtils.getPreviousDataStatus(getActivity())== true) {
//            buildingService = BuildingService.getInstance(PrefUtils.getThrownIdForDBSearching(getActivity()));
//        }
//        else buildingService = new BuildingService();


        inflatedView = inflater.inflate(R.layout.fragment_card_base, container, false);

        // Create a Card
        Card upperCard = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_asset_utama);

        CardView cardView = (CardView) inflatedView.findViewById(R.id.upperCard);
        cardView.setCard(upperCard);

//        bulkDataLoader.floodEditTextWithWords(inflatedView, R.id.layoutAssetUtama);

        saveButton = (Button) inflatedView.findViewById(R.id.simpanDataButton);
        uploadButton = (Button) inflatedView.findViewById(R.id.uploadDataButton);

        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                doSave();

            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                doSave();

            }
        });

        return inflatedView;
    }

    public void doSave() {

    }

}
