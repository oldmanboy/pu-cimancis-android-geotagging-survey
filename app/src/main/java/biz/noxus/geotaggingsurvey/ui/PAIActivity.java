package biz.noxus.geotaggingsurvey.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import biz.noxus.geotaggingsurvey.adapter.SmartFragmentStatePagerAdapter;
import biz.noxus.geotaggingsurvey.helper.Communicator;
import biz.noxus.geotaggingsurvey.ui.fragment.AssetTambahDataUtamaFragment;
import biz.noxus.geotaggingsurvey.ui.fragment.PAIDataUtamaFragment;
import biz.noxus.geotaggingsurvey.ui.fragment.PAIKoordinatFragment;

public class PAIActivity extends SurveyorActivity implements Communicator{
    private SmartFragmentStatePagerAdapter PAIAdapterViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragmentManager = getSupportFragmentManager();


        viewPager.setAdapter(PAIAdapterViewPager = new PAIStatePagerAdapter(fragmentManager));


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

//                BaseDataUtamaFragment DataUtamaFragment;
//                DataUtamaFragment = (BaseDataUtamaFragment) PAIAdapterViewPager.getRegisteredFragment(0);
//                DataUtamaFragment.saveDataUtamaFragmentInputToPref();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void initiateAssetTambahDataUtama() {
        AssetTambahDataUtamaFragment DataUtamaFragment;
        DataUtamaFragment = (AssetTambahDataUtamaFragment) PAIAdapterViewPager.getRegisteredFragment(0);
        DataUtamaFragment.processThisFragment();
    }
}

class PAIStatePagerAdapter extends SmartFragmentStatePagerAdapter {
    private static int NUM_ITEMS = 2;


    public PAIStatePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PAIDataUtamaFragment();
            case 1:
                return new PAIKoordinatFragment();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Form PAI";
            case 1:
                return "Pengambilan Koordinat";
             default:
                return null;
        }

    }


}