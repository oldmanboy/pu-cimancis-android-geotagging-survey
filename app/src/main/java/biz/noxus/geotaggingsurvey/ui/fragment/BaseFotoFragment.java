package biz.noxus.geotaggingsurvey.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import biz.noxus.geotaggingsurvey.R;
import biz.noxus.geotaggingsurvey.helper.Communicator;
import biz.noxus.geotaggingsurvey.helper.JSONParser;
import biz.noxus.geotaggingsurvey.model.Building;
import biz.noxus.geotaggingsurvey.service.BuildingService;
import biz.noxus.geotaggingsurvey.service.GeoObjectService;
import biz.noxus.geotaggingsurvey.service.UploaderService;
import biz.noxus.geotaggingsurvey.utils.PrefUtils;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.view.CardView;

public class BaseFotoFragment extends Fragment {
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    public static String thrownIdForDB;

    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "PU_Cimancis";

//    // JSON Node names
//    private static final String TAG_SUCCESS = "success";
//
//    // url to create new product
//    private static String URL_CREATE_GEOOBJECT = "http://noxus.biz/playground/cimancis/tambah.php";

    // local state container
    private static Boolean HAS_PREVIOUS_DATA;
    private static String SELECTED_BUILDING_ID;
    private static Boolean HAS_SAVED_ONCE;


    public Object selectedImagePreviewObject, selectedPlaceholderObject;

    public String SELECTED_FILE_URI = "";

    public String processURI;

    Communicator communicator;

    BuildingService buildingService = new BuildingService();
    List<Building> theBuildingInQuestion;

    HashMap inputtedDataMap = new HashMap();

    View viewOfThisFragment;

    Button saveButton, uploadButton;

    // Related with camera
    Uri cameraImg1, cameraImg2, cameraImg3, cameraImg4;
    String imgURI1, imgURI2, imgURI3, imgURI4;
    Integer selectedButtonIndex = null;


    // related with location based
    String LAT, LON, ELEVATION;
    Bitmap gambar1, gambar2, gambar3, gambar4;

    // related with upload
    JSONParser jsonParser = new JSONParser();

    String imgFilePath1, imgFilepath2, imgFilePath3;


    EditText inputNo;
    EditText inputKegiatan;
    EditText inputPelaksana;
    ImageView imgPreview1, imgPreview2, imgPreview3, imgPreview4, placeholder1, placeholder2, placeholder3, placeholder4;
    Button btnCapturePicture1, btnCapturePicture2, btnCapturePicture3,
            btnCapturePicture4;
    private String userLevel;
    private TextView keterangan;
    private LocationManager mLocationManager;
    // file url to store image/video
    private Uri currentUri;
    private VideoView videoPreview;
    // Progress Dialog
    private ProgressDialog pDialog;

    private LinearLayout addLayout, addLayoutInsideThecard;


    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Gagal membuat directory "
                        + IMAGE_DIRECTORY_NAME);
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
            Log.v("dbHandling", mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        //trying to intentStateCheck the returned status of the camera
//        if (savedInstanceState!=null) {
//            currentUri = savedInstanceState.getParcelable("file_uri");
//            Log.d("hasil kamera restore", currentUri.toString());
//        }

        this.HAS_SAVED_ONCE = false;
        PrefUtils.init(getActivity());
        HAS_PREVIOUS_DATA = PrefUtils.getPreviousDataStatus(getActivity());
        SELECTED_BUILDING_ID = PrefUtils.getThrownIdForDBSearching(getActivity());

        viewOfThisFragment = inflater.inflate(R.layout.fragment_card_base, container, false);

        // Create a Card
        Card card = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_camera);

        CardView cardView = (CardView) viewOfThisFragment.findViewById(R.id.upperCard);
        cardView.setCard(card);

        /*
        Setup for view elements used in this fragment
         */

        imgPreview1 = (ImageView) viewOfThisFragment.findViewById(R.id.imgPreview1);
        imgPreview2 = (ImageView) viewOfThisFragment.findViewById(R.id.imgPreview2);
        imgPreview3 = (ImageView) viewOfThisFragment.findViewById(R.id.imgPreview3);
        imgPreview4 = (ImageView) viewOfThisFragment.findViewById(R.id.imgPreview4);

        placeholder1 = (ImageView) viewOfThisFragment.findViewById(R.id.placeholder1);
        placeholder2 = (ImageView) viewOfThisFragment.findViewById(R.id.placeholder2);
        placeholder3 = (ImageView) viewOfThisFragment.findViewById(R.id.placeholder3);
        placeholder4 = (ImageView) viewOfThisFragment.findViewById(R.id.placeholder4);


        btnCapturePicture1 = (Button) viewOfThisFragment.findViewById(R.id.btnCapturePicture1);
        btnCapturePicture2 = (Button) viewOfThisFragment.findViewById(R.id.btnCapturePicture2);
        btnCapturePicture3 = (Button) viewOfThisFragment.findViewById(R.id.btnCapturePicture3);
        btnCapturePicture4 = (Button) viewOfThisFragment.findViewById(R.id.btnCapturePicture4);

        btnCapturePicture1.setText("Progress  0 %");
        btnCapturePicture2.setText("Progress  50 %");
        btnCapturePicture3.setText("Progress 100 %");
        btnCapturePicture4.setVisibility(View.GONE);

        placeholder4.setVisibility(View.GONE);

        /*
        -----end of setup
         */

        addLayout = (LinearLayout) viewOfThisFragment.findViewById(R.id.extraLayoutContainer);

        //tambah new layout apabila terpilih
        CardView tambahKartu = new CardView(getActivity());

        Card kartu = new Card(getActivity().getApplicationContext(), R.layout.fragment_view_save_stubs);

        addLayout.addView(tambahKartu);
        tambahKartu.setId(R.id.temporary_building_input_card_id);
        tambahKartu = (CardView) viewOfThisFragment.findViewById(R.id.temporary_building_input_card_id);
        tambahKartu.setCard(kartu);
        saveButton = (Button) viewOfThisFragment.findViewById(R.id.simpanDataButton);
        uploadButton = (Button) viewOfThisFragment.findViewById(R.id.uploadDataButton);

//        tambahKartu.setVisibility(View.INVISIBLE);
//        addLayoutInsideThecard = (LinearLayout) viewOfThisFragment.findViewById(R.id.blank_layout);

        /*
         * Capture image button click event
		 */
        btnCapturePicture1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                selectedImagePreviewObject = imgPreview1;
                selectedPlaceholderObject = placeholder1;
                captureImage();
                selectedButtonIndex = 1;
            }
        });

        btnCapturePicture2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                selectedImagePreviewObject = imgPreview2;
                selectedPlaceholderObject = placeholder2;
                captureImage();
                selectedButtonIndex = 2;
            }
        });

        btnCapturePicture3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                selectedImagePreviewObject = imgPreview3;
                selectedPlaceholderObject = placeholder3;
                captureImage();
                selectedButtonIndex = 3;
            }
        });

        btnCapturePicture4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                selectedImagePreviewObject = imgPreview4;
                selectedPlaceholderObject = placeholder4;
                captureImage();
                selectedButtonIndex = 4;
            }
        });

        if (HAS_PREVIOUS_DATA) {
            uploadButton.setVisibility(View.VISIBLE);
        }else {
            uploadButton.setVisibility(View.GONE);
        }



        // button click event
        if (HAS_PREVIOUS_DATA) saveButton.setText("Update Data");
        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                doSave();

            }
        });


        uploadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // creating new product in background thread
                //new CreateNewProduct().execute();

                //Tester for all saved data

                doSave();
                doUpload();

            }
        });

        if (HAS_PREVIOUS_DATA) {
            theBuildingInQuestion = buildingService.getOneBuilding(SELECTED_BUILDING_ID);
            restorePreviousDataToFragment();
        }


        return viewOfThisFragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("State Tracker", "Paused");
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (currentUri == null) {
            Log.i("State Tracker", "URI unavailable");
        } else {
            //operasi simpan ke database sementara
            //TODO lakukan null check lalu masukan ke database
            if (selectedButtonIndex == null) selectedButtonIndex = 1;

            switch (selectedButtonIndex) {
                case 1:
                    SELECTED_FILE_URI = "file_uri_1";
                    Log.i("State Tracker", "URI 1 saved");
                    break;
                case 2:
                    SELECTED_FILE_URI = "file_uri_2";
                    Log.i("State Tracker", "URI 2 saved");
                    break;
                case 3:
                    SELECTED_FILE_URI = "file_uri_3";
                    Log.i("State Tracker", "URI 3 saved");
                    break;
                case 4:
                    SELECTED_FILE_URI = "file_uri_4";
                    Log.i("State Tracker", "URI 4 saved");
                    break;

            }

            editor.putString(SELECTED_FILE_URI, currentUri.toString());
            editor.commit();
            Log.i("State Tracker", "URI saved");

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
        imgFilePath1 = sharedPreferences.getString("file_uri_1", "DEFAULT_VALUE");
        String imgFilePath2 = sharedPreferences.getString("file_uri_2", "DEFAULT_VALUE");
        String imgFilePath3 = sharedPreferences.getString("file_uri_3", "DEFAULT_VALUE");
        String imgFilePath4 = sharedPreferences.getString("file_uri_4", "DEFAULT_VALUE");

        if (imgFilePath1 != "DEFAULT_VALUE" || imgFilePath2 != "DEFAULT_VALUE" || imgFilePath3 != "DEFAULT_VALUE" || imgFilePath4 != "DEFAULT_VALUE") {
            Log.d("State Tracker", "some data has been saved");
            //TODO hasil kamera yang didapatkan diambil dari yang tidak null untuk ditampilkan di image view. bergantung pada storage chooser? belum ditemukan untuk switch? its bad to assume tapi mungkin sebaiknya dipakai

        }
        /*
        Log for state tracker when resumed
         */
        Log.i("State Tracker", "Resumed");
        Log.i("State Tracker of fileuri 1 is", imgFilePath1);
        Log.i("State Tracker of fileuri 2 is", imgFilePath2);
        Log.i("State Tracker of fileuri 3 is", imgFilePath3);
        Log.i("State Tracker of fileuri 4 is", imgFilePath4);

    }

    /**
     * Checking device has camera hardware or not
     */
    private boolean isDeviceSupportCamera() {
        if (getActivity().getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /*
     * Capturing Camera Image will launch camera app request image capture
     */
    public void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        currentUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        Log.d("Camera Output", currentUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, currentUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /*
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        Log.i("State Tracker", "save instance state triggered");
        outState.putParcelable("file_uri", currentUri);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image

        //currentUri = data.getExtras().getParcelable("file_uri");
        //Log.d("hasil parcel",currentUri.toString());

        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                // successfully captured the image
                // display it in image view
                previewCapturedImage();

            } else if (resultCode == getActivity().RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getActivity().getApplicationContext(),
                        "Pengambilan gambar batal", Toast.LENGTH_SHORT).show();
            } else {
                // failed to capture image
                Toast.makeText(getActivity().getApplicationContext(),
                        "Pengambilan gambar gagal", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                // video successfully recorded
                // preview the recorded video
                // previewVideo();
            } else if (resultCode == getActivity().RESULT_CANCELED) {
                // user cancelled recording
                Toast.makeText(getActivity().getApplicationContext(),
                        "User cancelled video recording", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to record video
                Toast.makeText(getActivity().getApplicationContext(),
                        "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    /*
     * Display image from a path to ImageView Groups
     */
    private void previewCapturedImage() {
        try {
            // hide and show preview

            ((View) selectedPlaceholderObject).setVisibility(View.GONE);
            ((View) selectedImagePreviewObject).setVisibility(View.VISIBLE);

            // bitmap factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;


            // String intentStateCheck = currentUri.getPath();
            // String imgPath = data.toString();
            // Choose to where the picture is saved
            switch (selectedButtonIndex) {
                case 1:
                    cameraImg1 = currentUri;
                    imgURI1 = cameraImg1.getPath();
                    break;
                case 2:
                    cameraImg2 = currentUri;
                    imgURI2 = cameraImg2.getPath();
                    break;
                case 3:
                    cameraImg3 = currentUri;
                    imgURI3 = cameraImg3.getPath();
                    break;
                case 4:
                    cameraImg4 = currentUri;
                    imgURI4 = cameraImg4.getPath();
                    break;

                default:
                    break;
            }

            // Log.v("dbHandling",intentStateCheck);

            final Bitmap bitmap = BitmapFactory.decodeFile(currentUri.getPath(),
                    options);

            ((ImageView) selectedImagePreviewObject).setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * ------------ Helper Methods ----------------------
     */

	/*
     * Creating file uri to store image/video
	 */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    public void doSave() {
        //TODO: save data dari building from data utama fragment, get geoobject id & group id dari fragment kemudian simpan hasil foto dr foto fragment ke db
        //TODO: buat cek apakah dia sudah ada data sebelumnya atau tidak.
        hashMapPackager();
        // TODO:insert hashmap from sharedPrefs to be processed at db level
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();


        if (HAS_PREVIOUS_DATA) {
            //do update
            buildingService = BuildingService.getInstance();
            buildingService.setBuildingModelInstanceFromSelectedId(SELECTED_BUILDING_ID);
            buildingService.updateBuildingData(inputtedDataMap);

            editor.putString("setThrownIdForDBSearching", buildingService.getCurrentBuildingId().toString());
            editor.commit();


        } else if (!this.HAS_SAVED_ONCE) {
            buildingService.addMoreBuilding(inputtedDataMap);

            editor.putString("setThrownIdForDBSearching", buildingService.getCurrentBuildingId().toString());
            editor.commit();
            this.HAS_SAVED_ONCE = true;


        }

        Log.d("just saved id", buildingService.getCurrentBuildingId().toString());
        Log.d("thrown id is", sharedPreferences.getString("setThrownIdForDBSearching", null));

        buildingService.bulkSavingMultipleFieldsToMutipleColumnInTable(sharedPreferences);
        Toast.makeText(getActivity().getApplicationContext(), "Data Tersimpan", Toast.LENGTH_SHORT).show();

        /*experimental
        Intent i = new Intent(getActivity().getApplicationContext(), SurveyorActivity.class);

        Bundle b = new Bundle();
        b.putString("itemFromDatabaseIndex", buildingService.getCurrentBuildingId().toString());
        i.putExtras(b);
        startActivity(i);
*/

    }

    public void doUpload() {

        buildingService = BuildingService.getInstance();
        buildingService.setBuildingUploaded();

        GeoObjectService geoObjectService = GeoObjectService.getInstance();


        buildingService.getAllBuilding();
        geoObjectService.getAllGeoObject();

//        Toast.makeText(getActivity().getApplicationContext(), geoObjectService.listOfAllGeoObjectStoredLocally, Toast.LENGTH_LONG).show();
        UploaderService progressUploader = new UploaderService(getActivity().getApplicationContext());
        progressUploader.POST_URL = "http://202.162.214.236/geotag/app/remote-add-progress.php";
        progressUploader.stringToSend = buildingService.listOfBuildingToUpload();
        progressUploader.uploader();

//        Toast.makeText(getActivity().getApplicationContext(), buildingService.listOfAllBuildingStoredLocally, Toast.LENGTH_LONG).show();
        UploaderService geotagUploader = new UploaderService(getActivity().getApplicationContext());
        geotagUploader.POST_URL = "http://202.162.214.236/geotag/app/remote-add.php";
        geotagUploader.stringToSend = geoObjectService.listOfAllGeoObjectStoredLocally; //this sends all the geo object. it should just send that hooks with this particular pelaksana
        geotagUploader.uploader();

        if (thrownIdForDB == "-") thrownIdForDB = buildingService.getCurrentBuildingId().toString();

        Log.d("now the utils:", PrefUtils.getThrownIdForDBSearching(getActivity()));
        Log.d("now the building id:", buildingService.getCurrentBuildingId().toString());

        Cursor testCursor = buildingService.fetchResultCursor("_id = " + buildingService.getCurrentBuildingId().toString());
        testCursor.moveToFirst(); //what is this?
        buildingService.bulkUploadMultipleFields(testCursor);

        UploaderService progressUpdater = new UploaderService(getActivity().getApplicationContext());
        progressUpdater.POST_URL = "http://202.162.214.236/geotag/app/remote-add-progress-2.php";
        progressUpdater.stringToSend = buildingService.listOfTheFieldsToUpdate;
        progressUpdater.uploader();


//        ArrayList<String> listOfPhotoUri = new ArrayList<String>();

//        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
//        imgFilePath1 =  ((String) inputtedDataMap.get("fotoProgress1"));
//
//              listOfPhotoUri.add(imgFilePath1);
//                listOfPhotoUri.add(imgURI2);
//                listOfPhotoUri.add(imgURI3);


        try {


            processURI = (Uri.parse(imgFilePath1).getPath());
//        Toast.makeText(getActivity().getApplicationContext(), processURI, Toast.LENGTH_SHORT).show();

            Uri thisURI = Uri.parse(processURI);


            BitmapFactory.Options bmoptions = new BitmapFactory.Options();
            bmoptions.inSampleSize = 6;
            Bitmap bitmap = BitmapFactory.decodeFile(processURI, bmoptions);

            String POST_URL = "http://202.162.214.236/geotag/app/uploader.php";


            uploadImage(POST_URL, bitmap, Uri.parse(imgFilePath1).getLastPathSegment());

            processURI = (Uri.parse(imgFilepath2).getPath());
            bitmap = BitmapFactory.decodeFile(processURI, bmoptions);

            uploadImage(POST_URL, bitmap, Uri.parse(imgFilepath2).getLastPathSegment());

            processURI = (Uri.parse(imgFilePath3).getPath());
            bitmap = BitmapFactory.decodeFile(processURI, bmoptions);

            uploadImage(POST_URL, bitmap, Uri.parse(imgFilePath3).getLastPathSegment());
        } catch (Exception e) {

        }

    }

    public void saveFotoFromFragmentToPref() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("tipeBangunan", theBuildingInQuestion.get(0).buildingType);
        editor.putInt("currentGroupObjectId", theBuildingInQuestion.get(0).geoObjectGroupId);
        editor.putString("file_uri_1", theBuildingInQuestion.get(0).fotoProgressBeginning);
        editor.putString("file_uri_2", theBuildingInQuestion.get(0).fotoProgressMiddle);
        editor.putString("file_uri_3", theBuildingInQuestion.get(0).fotoProgressEnd);
        editor.commit();

    }

    public void hashMapPackager() {
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(PrefUtils.getPrefName(), Context.MODE_PRIVATE);

        inputtedDataMap.put("tipeBangunan", sharedPreferences.getString("tipeBangunan", "Belum Terpilih"));
        inputtedDataMap.put("groupObjectId", sharedPreferences.getInt("currentGroupObjectId", 0));
        inputtedDataMap.put("fotoProgress1", sharedPreferences.getString("file_uri_1", "NO_PICTURE"));
        inputtedDataMap.put("fotoProgress2", sharedPreferences.getString("file_uri_2", "NO_PICTURE"));
        inputtedDataMap.put("fotoProgress3", sharedPreferences.getString("file_uri_3", "NO_PICTURE"));
        inputtedDataMap.put("pelaksana", PrefUtils.getLoggedInUser(getActivity().getApplicationContext()));

    }

    public void restorePreviousDataToFragment() {
        String fotoProgressBeginning = theBuildingInQuestion.get(0).fotoProgressBeginning;
        String fotoProgressMiddle = theBuildingInQuestion.get(0).fotoProgressMiddle;
        String fotoProgressEnd = theBuildingInQuestion.get(0).fotoProgressEnd;

        imgFilePath1 = fotoProgressBeginning;
        imgFilepath2 = fotoProgressMiddle;
        imgFilePath3 = fotoProgressEnd;

        setPreviewImageFromDB(imgPreview1, placeholder1, fotoProgressBeginning);
        setPreviewImageFromDB(imgPreview2, placeholder2, fotoProgressMiddle);
        setPreviewImageFromDB(imgPreview3, placeholder3, fotoProgressEnd);

        saveFotoFromFragmentToPref();
    }

    public void setPreviewImageFromDB(ImageView selectedImgPreview, ImageView selectedImgPlaceholder, String rawImagePath) {
        try {
            // show preview
            selectedImgPlaceholder.setVisibility(View.GONE);
            selectedImgPreview.setVisibility(View.VISIBLE);

            // bitmap factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger images
            options.inSampleSize = 8;

            // Choose to where the picture is saved
            currentUri = Uri.parse(rawImagePath);

            final Bitmap bitmap = BitmapFactory.decodeFile(currentUri.getPath(),
                    options);

            selectedImgPreview.setImageBitmap(bitmap);

//            Toast.makeText(getActivity(),
//                    "Loaded Image View with path :" + rawImagePath, Toast.LENGTH_SHORT).show();

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void uploadImage(String UPLOAD_URL, final Bitmap selectedImageBitmap, String name) {

        final String KEY_IMAGE = "image";
        final String KEY_NAME = "name";
        final String TheName = name;

        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Uploading...", "Please wait...", false, false);

        StringRequest stringRequest = null;
        try {
            stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            loading.dismiss();
                            //Showing toast message of the response
                            Toast.makeText(getActivity().getApplicationContext(), s, Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            loading.dismiss();

                            //Showing toast
//                            Toast.makeText(getActivity().getApplicationContext(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String
                    String image = getStringImage(selectedImageBitmap);

                    //Creating parameters
                    Map<String, String> params = new Hashtable<String, String>();

                    //Adding parameters
                    params.put(KEY_IMAGE, image);
                    params.put(KEY_NAME, TheName);

                    //returning parameters
                    return params;
                }
            };
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

}
